# frozen_string_literal: true

module Gitlab
  module SecretDetection
    class Gem
      # TODO: This is a temporary fix to avoid runtime issues
      # More details are available here:
      #  https://gitlab.com/gitlab-org/gitlab/-/issues/514015
      #
      # Ensure to maintain the same version in CHANGELOG file.
      VERSION = "0.20.4"

      # SD_ENV env var is used to determine which environment the
      # server is running. This var is defined in `.runway/env-<env>.yml` files.
      def self.local_env?
        ENV.fetch('SD_ENV', 'localhost') == 'localhost'
      end
    end
  end
end
