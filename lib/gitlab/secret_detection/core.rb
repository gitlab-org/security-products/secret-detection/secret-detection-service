# frozen_string_literal: true

require_relative 'core/finding'
require_relative 'core/response'
require_relative 'core/status'
require_relative 'core/scanner'
require_relative 'core/ruleset'

module Gitlab
  module SecretDetection
    module Core
    end
  end
end
