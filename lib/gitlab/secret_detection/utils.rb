# frozen_string_literal: true

require_relative 'utils/certificate'
require_relative 'utils/memoize'
require_relative 'utils/masker'

module Gitlab
  module SecretDetection
    module Utils
    end
  end
end
