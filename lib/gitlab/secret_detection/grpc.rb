# frozen_string_literal: true

require_relative 'grpc/integrated_error_tracking'
require_relative 'grpc/scanner_service'
require_relative 'grpc/client/stream_request_enumerator'
require_relative 'grpc/client/grpc_client'

module Gitlab
  module SecretDetection
    module GRPC
    end
  end
end
