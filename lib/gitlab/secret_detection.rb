# frozen_string_literal: true

require_relative 'secret_detection/utils'
require_relative 'secret_detection/core'
require_relative 'secret_detection/grpc'
require_relative 'secret_detection/version'

module Gitlab
  module SecretDetection
  end
end
