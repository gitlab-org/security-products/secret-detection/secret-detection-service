# frozen_string_literal: true

require 'grpc'
require 'grpc/health/v1/health_pb'
require 'grpc/health/v1/health_services_pb'
require 'grpc/health/checker'
require 'grpc_reflection'

require_relative '../config/log'
require_relative '../lib/gitlab/secret_detection/grpc'
require_relative 'interceptors/auth_interceptor'
require_relative 'interceptors/log_interceptor'

module Gitlab
  module SecretDetection
    module GRPC
      class Server
        include SDLogger

        DEFAULT_RPC_SERVER_PORT = 50001

        # The amount of time in seconds to wait
        # for currently busy thread-pool threads to finish before forcing an abrupt exit to each thread.
        DEFAULT_POOL_KEEP_ALIVE_IN_SECONDS = 2

        # Size of the threadpool to achieve concurrency. Set it to same value
        # as Cloud Run's threshold (https://cloud.google.com/run/docs/about-concurrency#case-study)
        MAX_CONCURRENCY = 80

        def initialize
          @server = ::GRPC::RpcServer.new(
            pool_keep_alive: DEFAULT_POOL_KEEP_ALIVE_IN_SECONDS,
            pool_size: MAX_CONCURRENCY,
            interceptors: server_interceptors,
            server_args: channel_server_args
          )
          configure_grpc_server
          initialize_error_tracking
        end

        def start_server
          logger.info "GRPC server running on port: #{port}"
          @server.run_till_terminated_or_interrupted([1, 'int', 'SIGTERM'])
        end

        def stop_server
          logger.info "stopping GRPC server"
          @server.stop unless @server.stopped?
        end

        private

        def initialize_error_tracking
          Gitlab::SecretDetection::GRPC::IntegratedErrorTracking.setup(logger:)
        end

        def configure_grpc_server
          @server.add_http2_port("0.0.0.0:#{port}", :this_port_is_insecure)

          @server.handle(GrpcReflection::Server) # GRPC Reflection service
          @server.handle(GRPC::ScannerService) # Secret Detection service
          @server.handle(health_checker_service) # Health Check service
        end

        def port
          ENV['RPC_SERVER_PORT'] || DEFAULT_RPC_SERVER_PORT
        end

        def health_checker_service
          # health check
          status_serving = ::Grpc::Health::V1::HealthCheckResponse::ServingStatus::SERVING
          health_checker = ::Grpc::Health::Checker.new
          # Ideally we should determine the status based on checking DB connections, etc.
          # Since this service is a stateless service independent of any other dependencies, we'll mark it as serving.
          health_checker.add_status '', status_serving # for overall system status
          health_checker.add_status GRPC::ScannerService.service_name, status_serving # scanner svc status

          health_checker
        end

        # all the interceptors for RPC server
        def server_interceptors
          [
            AuthInterceptor.new,
            LogInterceptor.new
          ].freeze
        end

        def channel_server_args
          {
            'grpc.keepalive_permit_without_calls' => 1, # Allow keepalives without active calls
            'grpc.keepalive_time_ms' => 30000, # 30 seconds (how often to send pings)
            'grpc.keepalive_timeout_ms' => 10000 # 10 seconds (ping timeout)
          }
        end
      end
    end
  end
end
