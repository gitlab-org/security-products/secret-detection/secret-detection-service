# frozen_string_literal: true

require 'grpc'

require_relative './base_interceptor'

module Gitlab
  module SecretDetection
    module GRPC
      class LogInterceptor < Gitlab::SecretDetection::GRPC::BaseInterceptor
        include SDLogger

        GRPCStatus = ::GRPC::Core::StatusCodes

        # GRPC Status codes mapped to readable string literals
        STATUS_CODES = {
          GRPCStatus::OK => 'OK',
          GRPCStatus::CANCELLED => 'Cancelled',
          GRPCStatus::UNKNOWN => 'Unknown',
          GRPCStatus::INVALID_ARGUMENT => 'Invalid Argument',
          GRPCStatus::DEADLINE_EXCEEDED => 'Deadline Exceeded',
          GRPCStatus::NOT_FOUND => 'Not Found',
          GRPCStatus::ALREADY_EXISTS => 'Already Exists',
          GRPCStatus::PERMISSION_DENIED => 'Permission Denied',
          GRPCStatus::RESOURCE_EXHAUSTED => 'Resource Exhausted',
          GRPCStatus::FAILED_PRECONDITION => 'Failed Precondition',
          GRPCStatus::ABORTED => 'Aborted',
          GRPCStatus::OUT_OF_RANGE => 'Out Of Range',
          GRPCStatus::UNIMPLEMENTED => 'Unimplemented',
          GRPCStatus::INTERNAL => 'Internal',
          GRPCStatus::UNAVAILABLE => 'Unavailable',
          GRPCStatus::DATA_LOSS => 'Data Loss',
          GRPCStatus::UNAUTHENTICATED => 'Unauthenticated'
        }.freeze

        # Custom request header prefix that is checked for its presence
        # when logging the request header
        CUSTOM_HEADER_PREFIX = 'x-'

        # Intercept a unary call
        def request_response(request: nil, call: nil, method: nil, &block)
          log_request(request, method, call, &block)
        end

        # Intercept a client streaming call
        def client_streamer(requests: nil, call: nil, method: nil, &block)
          log_request(requests, method, call, &block)
        end

        # Intercept a server streaming call
        def server_streamer(requests: nil, call: nil, method: nil, &block)
          log_request(requests, method, call, &block)
        end

        # Intercept a Bi-directional streaming call
        def bidi_streamer(requests: nil, call: nil, method: nil, &block)
          log_request(requests, method, call, &block)
        end

        private

        # takes the request information and logs the necessary
        # information
        def log_request(_requests, method, call)
          received_at = Time.now.utc
          status_code = GRPCStatus::OK
          # append custom header values prefixed with `X-`
          headers = custom_req_headers(call) unless reflection_api?(method)

          yield
        rescue StandardError => e
          status_code = e.is_a?(::GRPC::BadStatus) ? e.code : GRPCStatus::UNKNOWN
          raise
        ensure

          payload = {
            grpc_method: "#{service_name(method)}/#{method_name(method)}",
            status_code: STATUS_CODES.fetch(status_code, status_code.to_s),
            duration_ms: elapsed_time_ms(received_at)
          }

          payload.merge!(headers) unless headers.nil?

          if e.nil?
            logger.info(message: "gRPC Request summary", payload:) unless reflection_api?(method)
          else
            unless reflection_api?(method)
              logger.error(
                message: "gRPC Request failure",
                payload:,
                exception: e.message
              )
            end
          end
        end

        # returns elapsed time in milliseconds
        def elapsed_time_ms(start_time)
          ((Time.now.utc - start_time) * 1000).to_i
        end

        # Pulls all the headers from the request that
        # prefixes with `X-`(case insensitive) except Auth token header.
        # This is useful to log custom headers like Request ID or Correlation ID.
        def custom_req_headers(call)
          headers = {}

          call.metadata&.each do |key, value|
            header = key.downcase

            next unless header.start_with?(CUSTOM_HEADER_PREFIX)

            headers[header] = value
          end

          # do not log auth token
          headers.delete AuthInterceptor::REQUEST_AUTH_HEADER

          headers.freeze
        end
      end
    end
  end
end
