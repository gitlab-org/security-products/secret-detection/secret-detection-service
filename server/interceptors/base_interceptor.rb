# frozen_string_literal: true

require 'grpc'
require 'logger'

require_relative '../../config/log'

module Gitlab
  module SecretDetection
    module GRPC
      class BaseInterceptor < ::GRPC::Interceptor
        include SDLogger

        # When invoking the RPC endpoints, some RPC clients call
        # server reflection API to determine the available RPC methods.
        # We do not want to log it.
        def reflection_api?(method)
          method.name == :server_reflection_info
        end

        # fetches method name defined in the service definition file
        def method_name(method)
          owner = method.owner

          method_name = 'UNKNOWN'

          owner.rpc_descs.each_key do |svc|
            if ::GRPC::GenericService.underscore(svc.to_s).to_sym == method.name.to_sym
              method_name = svc.to_s
              break
            end
          end

          method_name
        end

        # returns name of the RPC service
        def service_name(method)
          method.owner.service_name
        end

        # SD_ENV env var is used to determine which environment the
        # server is running. This var is defined in `.runway/env-<env>.yml` files.
        def local_env?
          ENV.fetch('SD_ENV', 'localhost') == 'localhost'
        end
      end
    end
  end
end
