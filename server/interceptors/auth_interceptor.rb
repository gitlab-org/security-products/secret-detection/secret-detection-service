# frozen_string_literal: true

require 'grpc'

require_relative 'base_interceptor'

module Gitlab
  module SecretDetection
    module GRPC
      class AuthInterceptor < Gitlab::SecretDetection::GRPC::BaseInterceptor
        # Name of the environment variable where the Auth API key is stored.
        # This key+value is injected by Runway fetched from Vault
        ENV_AUTH_TOKEN_NAME = 'API_AUTH_TOKEN'

        # Name of the RPC request header where the client should embed the
        # Auth token in the request
        REQUEST_AUTH_HEADER = 'x-sd-auth'

        # Auth token for a local server as token is injected only
        # in staging and production environments.
        LOCALHOST_AUTH_TOKEN = '12345'

        # RPC service for Secret Detection scanner
        SECRET_DETECTION_RPC_SERVICE = 'gitlab.secret_detection.Scanner'

        def initialize(server_auth_token: fetch_server_auth_token)
          @server_auth_token = server_auth_token
        end

        # Intercept a unary call
        def request_response(request: nil, call: nil, method: nil, &block)
          run_with_auth(request, call, method, &block)
        end

        # Intercept a client streaming call
        def client_streamer(requests: nil, call: nil, method: nil, &block)
          run_with_auth(requests, call, method, &block)
        end

        # Intercept a server streaming call
        def server_streamer(requests: nil, call: nil, method: nil, &block)
          run_with_auth(requests, call, method, &block)
        end

        # Intercept a Bi-directional streaming call
        def bidi_streamer(requests: nil, call: nil, method: nil, &block)
          run_with_auth(requests, call, method, &block)
        end

        private

        attr_reader :server_auth_token

        # Runs the request(s) through authentication process
        def run_with_auth(_requests, call, method)
          raise ::GRPC::Unauthenticated, "Authentication failed" unless authenticated?(call, method)

          yield
        end

        # Fetches Auth API key from ENV. Raises +::GRPC::Internal+ error if it is not present
        def fetch_server_auth_token
          return LOCALHOST_AUTH_TOKEN if local_env?

          api_key = ENV[ENV_AUTH_TOKEN_NAME]

          raise ::GRPC::Internal, 'API_AUTH_TOKEN not configured in the env vars' if api_key.nil?

          api_key
        end

        # Checks for auth token in the request header. Returns
        # true if either API request is Server Reflection API or
        # header value matches server token value.
        def authenticated?(call, method)
          # run authentication only on Scanner service but
          # not health check or reflection services
          return true unless scanner_service_request?(method)

          server_auth_token == call.metadata.fetch(REQUEST_AUTH_HEADER, '')
        end

        # returns if the RPC request is for Secret Scanner service
        def scanner_service_request?(method)
          service_name(method) == SECRET_DETECTION_RPC_SERVICE
        end
      end
    end
  end
end
