# frozen_string_literal: true

require 'rspec'
require 'rspec-parameterized'
require 'rspec-benchmark'
require 'benchmark-malloc'
require 'simplecov'

SimpleCov.start

require_relative './support/grpc_helper'
require_relative '../lib/gitlab/secret_detection'

SimpleCov.start

RSpec.configure do |config|
  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.include RSpec::Benchmark::Matchers
  config.include Gitlab::GRPC::TestHelper
  config.example_status_persistence_file_path = ENV.fetch('RSPEC_LAST_RUN_RESULTS_FILE', './spec/examples.txt')

  Dir['./spec/support/**/*.rb'].each { |f| require f }

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  # configure benchmark factors
  RSpec::Benchmark.configure do |cfg|
    # to avoid retention of allocated memory by the perf tests in the main process
    cfg.run_in_subprocess = true
  end
end
