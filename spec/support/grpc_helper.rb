# frozen_string_literal: true

require 'grpc'
require 'logger'
require 'grpc/health/v1/health_pb'
require 'grpc/health/v1/health_services_pb'
require 'grpc/health/checker'

require_relative '../../lib/gitlab/secret_detection/grpc'

module Gitlab
  module GRPC
    module TestHelper
      TEST_SERVER_PORT = 50002

      def start_test_server(services, interceptors = [])
        @server = ::GRPC::RpcServer.new(
          interceptors:
        )
        @server.add_http2_port("0.0.0.0:#{TEST_SERVER_PORT}", :this_port_is_insecure)
        services.each do |service|
          @server.handle(service)
        end
        @server_thread = Thread.new { @server.run }
        @server.wait_till_running
      end

      def stop_test_server
        return if @server.nil? || !@server.running?

        @server.stop
        @server_thread.join
      end

      def stub_secret_detection_service
        Gitlab::SecretDetection::GRPC::Scanner::Stub.new("localhost:#{TEST_SERVER_PORT}", :this_channel_is_insecure)
      end

      def stub_health_check_service
        Grpc::Health::Checker.rpc_stub_class.new("localhost:#{TEST_SERVER_PORT}", :this_channel_is_insecure)
      end

      def create_health_check_service
        status_serving = Grpc::Health::V1::HealthCheckResponse::ServingStatus::SERVING
        health_checker = Grpc::Health::Checker.new
        health_checker.add_status '', status_serving # for overall system status
        health_checker
      end

      class RequestEnumerator
        def initialize(requests)
          @requests = requests
        end

        # yields a request, waiting between 0 and 1 seconds between requests
        #
        # @return an Enumerable that yields a request input
        def each_item
          return enum_for(:each_item) unless block_given?

          @requests.each do |request|
            yield request
          end
        end
      end
    end
  end
end
