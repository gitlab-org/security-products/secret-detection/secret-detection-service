# frozen_string_literal: true

require 'google/protobuf'

require_relative '../../../../spec_helper'

ScanService = Gitlab::SecretDetection::GRPC::ScannerService
ScanRequest = Gitlab::SecretDetection::GRPC::ScanRequest
ScanResponse = Gitlab::SecretDetection::GRPC::ScanResponse
ScanStatus = Gitlab::SecretDetection::GRPC::ScanResponse::Status
CoreStatus = Gitlab::SecretDetection::Core::Status
CoreResponse = Gitlab::SecretDetection::Core::Response
CoreFinding = Gitlab::SecretDetection::Core::Finding

RSpec.describe Gitlab::SecretDetection::GRPC::ScannerService do
  before(:all) do
    services = [described_class]
    start_test_server(services)
  end

  let(:service) { stub_secret_detection_service }

  let(:payloads_without_secrets) do
    [
      ScanRequest::Payload.new(id: '101', data: "dummy data"),
      ScanRequest::Payload.new(id: '102', data: "dummy data 2")
    ]
  end

  let(:payloads_with_secrets) do
    [
      ScanRequest::Payload.new(id: '201', data: "glpat-#{'a' * 20}"),
      ScanRequest::Payload.new(id: '202', data: "GR1348941#{'a' * 20}")
    ]
  end

  let(:expected_response_for_secrets) do
    {
      results: [
        {
          payload_id: "201", status: ScanStatus::STATUS_FOUND, type: "gitlab_personal_access_token",
          description: "GitLab personal access token", line_number: 1
        },
        {
          payload_id: "202", status: ScanStatus::STATUS_FOUND, type: "gitlab_runner_registration_token",
          description: "GitLab runner registration token", line_number: 1
        }
      ],
      status: ScanStatus::STATUS_FOUND,
      applied_exclusions: []
    }
  end

  let(:expected_response_for_secrets_with_exclusions) do
    {
      results: [
        {
          payload_id: "201", status: ScanStatus::STATUS_FOUND, type: "gitlab_personal_access_token",
          description: "GitLab personal access token", line_number: 1
        }
      ],
      status: ScanStatus::STATUS_FOUND,
      applied_exclusions: rule_exclusions
    }
  end

  let(:expected_response_for_no_secrets) do
    {
      results: [],
      status: ScanStatus::STATUS_NOT_FOUND,
      applied_exclusions: []
    }
  end

  let(:rule_exclusions) do
    [
      Gitlab::SecretDetection::GRPC::Exclusion.new(
        value: 'gitlab_runner_registration_token',
        exclusion_type: Gitlab::SecretDetection::GRPC::ExclusionType::EXCLUSION_TYPE_RULE
      )
    ]
  end

  let(:mock_core_response_with_secrets) do
    findings = [
      CoreFinding.new("201", CoreStatus::FOUND, 1, "gitlab_personal_access_token", "GitLab personal access token"),
      CoreFinding.new("202", CoreStatus::FOUND, 1, "gitlab_runner_registration_token",
        "GitLab runner registration token")
    ]

    CoreResponse.new(
      status: CoreStatus::FOUND,
      results: findings,
      applied_exclusions: []
    )
  end

  let(:mock_core_response_without_secrets) do
    CoreResponse.new(
      status: CoreStatus::NOT_FOUND,
      results: [],
      applied_exclusions: []
    )
  end

  let(:mock_core_response_with_exclusions) do
    findings = [
      CoreFinding.new("201", CoreStatus::FOUND, 1, "gitlab_personal_access_token", "GitLab personal access token")
    ]

    CoreResponse.new(
      status: CoreStatus::FOUND,
      results: findings,
      applied_exclusions: rule_exclusions
    )
  end

  after(:all) do
    stop_test_server
  end

  describe "#scan" do
    context "when payloads contain secrets" do
      it "responds with detected findings" do
        allow_any_instance_of(Gitlab::SecretDetection::Core::Scanner).to receive(:secrets_scan)
          .and_return(mock_core_response_with_secrets)

        request = ScanRequest.new(payloads: payloads_with_secrets)
        expect(service.scan(request).to_h).to eq(expected_response_for_secrets)
      end

      context "when there are exclusions" do
        before do
          expected_response_for_secrets_with_exclusions[:applied_exclusions] = rule_exclusions.map(&:to_h)
        end

        it "responds with detected findings minus excluded ones" do
          allow_any_instance_of(Gitlab::SecretDetection::Core::Scanner).to receive(:secrets_scan)
            .and_return(mock_core_response_with_exclusions)

          request = ScanRequest.new(payloads: payloads_with_secrets, exclusions: rule_exclusions)
          expect(service.scan(request).to_h).to eq(expected_response_for_secrets_with_exclusions)
        end
      end
    end

    context "when payloads contain no secrets" do
      it "responds with STATUS_NOT_FOUND" do
        allow_any_instance_of(Gitlab::SecretDetection::Core::Scanner).to receive(:secrets_scan)
          .and_return(mock_core_response_without_secrets)

        request = ScanRequest.new(payloads: payloads_without_secrets)
        expect(service.scan(request).to_h).to eq(expected_response_for_no_secrets)
      end
    end

    context "when payloads are empty" do
      it "responds with STATUS_NOT_FOUND" do
        allow_any_instance_of(Gitlab::SecretDetection::Core::Scanner).to receive(:secrets_scan)
          .and_return(mock_core_response_without_secrets)

        request = ScanRequest.new(payloads: [])
        expect(service.scan(request).to_h).to eq(expected_response_for_no_secrets)
      end
    end

    context "when scan request is validated" do
      shared_examples "raises argument error" do |request, error_msg, err_field|
        it do
          expect { service.scan(request) }.to raise_error do |error|
            expect(error).to be_a(::GRPC::InvalidArgument)
            expect(error.details).to eq(described_class::ERROR_MESSAGES[error_msg])
            expect(error.metadata['field']).to eq(err_field)
          end
        end
      end

      context "when exclusion value is blank" do
        request = ScanRequest.new(exclusions: [{ exclusion_type: :EXCLUSION_TYPE_RAW_VALUE,
                                                 value: "" }])

        it_behaves_like "raises argument error", request, :exclusion_empty_value, "exclusion.value"
      end

      context "when scan timeout value is negative" do
        request = ScanRequest.new(timeout_secs: -20)

        it_behaves_like "raises argument error", request, :invalid_timeout_range, "timeout_secs"
      end

      context "when scan timeout value crosses allowed limit" do
        request = ScanRequest.new(timeout_secs: described_class::MAX_ALLOWED_TIMEOUT_SECONDS + 1)

        it_behaves_like "raises argument error", request, :invalid_timeout_range, "timeout_secs"
      end

      context "when payload timeout value is negative" do
        request = ScanRequest.new(payload_timeout_secs: -20)

        it_behaves_like "raises argument error", request, :invalid_timeout_range, "payload_timeout_secs"
      end

      context "when payload timeout value crosses allowed limit" do
        request = ScanRequest.new(
          payload_timeout_secs: described_class::MAX_ALLOWED_TIMEOUT_SECONDS + 1
        )

        it_behaves_like "raises argument error", request, :invalid_timeout_range, "payload_timeout_secs"
      end

      context "when payload has blank `id` value" do
        request = ScanRequest.new(
          payloads: [ScanRequest::Payload.new(id: "", data: "valid data")]
        )

        it_behaves_like "raises argument error", request, :invalid_payload_fields, "payloads[0].id"
      end
    end

    # New test cases
    context "when path exclusions are given" do
      let(:path_exclusions) do
        [
          Gitlab::SecretDetection::GRPC::Exclusion.new(
            value: 'some/path/*',
            exclusion_type: Gitlab::SecretDetection::GRPC::ExclusionType::EXCLUSION_TYPE_PATH
          )
        ]
      end

      it "sorts exclusions correctly by type and passes them to the scanner" do
        expect_any_instance_of(Gitlab::SecretDetection::Core::Scanner).to receive(
          :secrets_scan
        ) do |_instance, _payloads, options|
          expect(options[:exclusions][:path]).to eq(path_exclusions)
          mock_core_response_with_secrets
        end

        request = ScanRequest.new(payloads: payloads_with_secrets, exclusions: path_exclusions)
        service.scan(request)
      end
    end

    context "when unknown exclusion type is given" do
      let(:unknown_exclusions) do
        [
          Gitlab::SecretDetection::GRPC::Exclusion.new(
            value: 'test',
            exclusion_type: 999 # An invalid/unknown type
          )
        ]
      end

      it "logs a warning and continues processing" do
        allow_any_instance_of(Gitlab::SecretDetection::Core::Scanner).to receive(:secrets_scan)
          .and_return(mock_core_response_with_secrets)

        mock_logger = instance_double(Logger, warn: nil, info: nil)
        allow_any_instance_of(described_class).to receive(:logger).and_return(mock_logger)

        expect(mock_logger).to receive(:warn).with(/Unknown exclusion type/)
        allow(mock_logger).to receive(:info)

        request = ScanRequest.new(payloads: payloads_with_secrets, exclusions: unknown_exclusions)
        response = service.scan(request)

        # Verify the scan completes normally despite the unknown exclusion
        expect(response.status).to eq(ScanStatus::STATUS_FOUND)
      end
    end

    context "when scanner initialization fails" do
      it "handles the error and returns it as a GRPC error" do
        allow_any_instance_of(Gitlab::SecretDetection::Core::Scanner).to receive(:secrets_scan)
          .and_raise(StandardError, "Scanner initialization failed")

        request = ScanRequest.new(payloads: payloads_with_secrets)

        expect { service.scan(request) }.to raise_error do |error|
          expect(error).to be_a(GRPC::Unknown)
          expect(error.message).to include("Scanner initialization failed")
        end
      end
    end

    context "when valid timeout values are provided" do
      it "passes the timeout values to the scanner" do
        timeout_secs = 300
        payload_timeout_secs = 60

        expect_any_instance_of(Gitlab::SecretDetection::Core::Scanner).to receive(
          :secrets_scan
        ) do |_instance, _payloads, options|
          expect(options[:timeout]).to eq(timeout_secs)
          expect(options[:payload_timeout]).to eq(payload_timeout_secs)
          mock_core_response_with_secrets
        end

        request = ScanRequest.new(
          payloads: payloads_with_secrets,
          timeout_secs:,
          payload_timeout_secs:
        )

        service.scan(request)
      end
    end

    context "when multiple types of exclusions are provided" do
      let(:raw_exclusion) do
        Gitlab::SecretDetection::GRPC::Exclusion.new(
          value: "glpat-12312312312312312312",
          exclusion_type: Gitlab::SecretDetection::GRPC::ExclusionType::EXCLUSION_TYPE_RAW_VALUE
        )
      end

      let(:rule_exclusion) do
        Gitlab::SecretDetection::GRPC::Exclusion.new(
          value: "gitlab_runner_registration_token",
          exclusion_type: Gitlab::SecretDetection::GRPC::ExclusionType::EXCLUSION_TYPE_RULE
        )
      end

      let(:path_exclusion) do
        Gitlab::SecretDetection::GRPC::Exclusion.new(
          value: "some/path/*",
          exclusion_type: Gitlab::SecretDetection::GRPC::ExclusionType::EXCLUSION_TYPE_PATH
        )
      end

      let(:mixed_exclusions) { [raw_exclusion, rule_exclusion, path_exclusion] }

      let(:mock_response_with_mixed_exclusions) do
        CoreResponse.new(
          status: CoreStatus::FOUND,
          results: [CoreFinding.new("201", CoreStatus::FOUND, 1,
            "gitlab_personal_access_token", "GitLab personal access token")],
          applied_exclusions: mixed_exclusions
        )
      end

      it "sorts exclusions correctly by type" do
        expect_any_instance_of(Gitlab::SecretDetection::Core::Scanner).to receive(
          :secrets_scan
        ) do |_instance, _payloads, options|
          expect(options[:exclusions][:raw_value]).to include(raw_exclusion)
          expect(options[:exclusions][:rule]).to include(rule_exclusion)
          expect(options[:exclusions][:path]).to include(path_exclusion)
          mock_response_with_mixed_exclusions
        end

        request = ScanRequest.new(payloads: payloads_with_secrets, exclusions: mixed_exclusions)
        service.scan(request)
      end
    end

    context "when tags are provided" do
      let(:tags) { %w[gitlab security] }

      it "passes tags to the scanner" do
        expect_any_instance_of(Gitlab::SecretDetection::Core::Scanner).to receive(
          :secrets_scan
        ) do |_instance, _payloads, options|
          expect(options[:tags]).to eq(tags)
          mock_core_response_with_secrets
        end

        request = ScanRequest.new(payloads: payloads_with_secrets, tags:)
        service.scan(request)
      end
    end

    context "with special payloads" do
      it "handles payloads with special characters" do
        special_payload = ScanRequest::Payload.new(
          id: '999',
          data: "Data with unicode \u{1F600} and special chars !@#$%^&*()"
        )

        # Should not raise errors
        expect { service.scan(ScanRequest.new(payloads: [special_payload])) }.not_to raise_error
      end

      it "handles very large payloads within reasonable limits" do
        large_data = "x" * 10_000 # 10KB of data, reduced from 1MB for spec performance
        large_payload = ScanRequest::Payload.new(id: 'large', data: large_data)

        # Should complete without timeout errors when using default timeout
        expect { service.scan(ScanRequest.new(payloads: [large_payload])) }.not_to raise_error
      end
    end
  end

  describe "#scan_stream" do
    context "for a given stream of payloads" do
      context "when all payloads contain secrets" do
        it "responds with detected findings for each stream request" do
          allow_any_instance_of(Gitlab::SecretDetection::Core::Scanner).to receive(:secrets_scan)
            .and_return(mock_core_response_with_secrets)

          requests = Array.new(3) { ScanRequest.new(payloads: payloads_with_secrets) }
          request_stream = Gitlab::GRPC::TestHelper::RequestEnumerator.new(requests)

          expect(service.scan_stream(request_stream.each_item)).to satisfy do |stream_result|
            stream_result.each do |result|
              expect(result.to_h).to eql(expected_response_for_secrets)
            end
          end
        end
      end

      context "when no payloads contain secrets" do
        it "responds with STATUS_NOT_FOUND for each stream request" do
          allow_any_instance_of(Gitlab::SecretDetection::Core::Scanner).to receive(:secrets_scan)
            .and_return(mock_core_response_without_secrets)

          requests = Array.new(3) { ScanRequest.new(payloads: payloads_without_secrets) }
          request_stream = Gitlab::GRPC::TestHelper::RequestEnumerator.new(requests)

          expect(service.scan_stream(request_stream.each_item)).to satisfy do |stream_result|
            stream_result.each do |result|
              expect(result.to_h).to eql(expected_response_for_no_secrets)
            end
          end
        end
      end

      context "when one of stream messages contain payloads with secrets" do
        it "processes each message separately" do
          # Mock scanner to return different responses for different payloads
          allow_any_instance_of(Gitlab::SecretDetection::Core::Scanner).to receive(
            :secrets_scan
          ) do |_instance, payloads, _options|
            if payloads == payloads_with_secrets
              mock_core_response_with_secrets
            else
              mock_core_response_without_secrets
            end
          end

          requests = [
            ScanRequest.new(payloads: payloads_without_secrets),
            ScanRequest.new(payloads: payloads_with_secrets),
            ScanRequest.new(payloads: payloads_without_secrets)
          ]

          request_stream = Gitlab::GRPC::TestHelper::RequestEnumerator.new(requests)
          expected_response = [
            expected_response_for_no_secrets,
            expected_response_for_secrets,
            expected_response_for_no_secrets
          ]

          expect(service.scan_stream(request_stream.each_item)).to satisfy do |stream_result|
            stream_result.each_with_index do |result, index|
              expect(result.to_h).to eql(expected_response[index])
            end
          end
        end
      end

      context "when no payloads are passed" do
        it "responds with STATUS_NOT_FOUND" do
          request = ScanRequest.new(payloads: [])
          request_stream = Gitlab::GRPC::TestHelper::RequestEnumerator.new([request])
          expect(service.scan_stream(request_stream.each_item)).to satisfy do |stream_result|
            stream_result.each do |result|
              expect(result.to_h).to eql(expected_response_for_no_secrets)
            end
          end
        end
      end

      # New test for scan_stream
      context "when stream messages contain different exclusion types" do
        let(:rule_exclusion1) do
          Gitlab::SecretDetection::GRPC::Exclusion.new(
            value: "gitlab_personal_access_token",
            exclusion_type: Gitlab::SecretDetection::GRPC::ExclusionType::EXCLUSION_TYPE_RULE
          )
        end

        let(:rule_exclusion2) do
          Gitlab::SecretDetection::GRPC::Exclusion.new(
            value: "gitlab_runner_registration_token",
            exclusion_type: Gitlab::SecretDetection::GRPC::ExclusionType::EXCLUSION_TYPE_RULE
          )
        end

        let(:mock_response1) do
          CoreResponse.new(
            status: CoreStatus::FOUND,
            results: [CoreFinding.new("202", CoreStatus::FOUND, 1,
              "gitlab_runner_registration_token", "GitLab runner registration token")],
            applied_exclusions: [rule_exclusion1]
          )
        end

        let(:mock_response2) do
          CoreResponse.new(
            status: CoreStatus::FOUND,
            results: [CoreFinding.new("201", CoreStatus::FOUND, 1,
              "gitlab_personal_access_token", "GitLab personal access token")],
            applied_exclusions: [rule_exclusion2]
          )
        end

        it "applies exclusions correctly for each message" do
          # Allow scanner to be called multiple times with different responses
          allow_any_instance_of(Gitlab::SecretDetection::Core::Scanner).to receive(:secrets_scan)
            .and_return(mock_response1, mock_response2)

          request1 = ScanRequest.new(
            payloads: payloads_with_secrets,
            exclusions: [rule_exclusion1]
          )

          request2 = ScanRequest.new(
            payloads: payloads_with_secrets,
            exclusions: [rule_exclusion2]
          )

          request_stream = Gitlab::GRPC::TestHelper::RequestEnumerator.new([request1, request2])

          # Process the stream and check that each message gets its own exclusions applied
          result_enum = service.scan_stream(request_stream.each_item)
          results = result_enum.to_a

          expect(results.length).to eq(2)
          expect(results[0].applied_exclusions[0].value).to eq("gitlab_personal_access_token")
          expect(results[1].applied_exclusions[0].value).to eq("gitlab_runner_registration_token")
        end
      end
    end

    context "when the streaming scan request is validated" do
      shared_examples "raises argument error when consuming stream" do |requests, err_msg, err_field|
        it do
          request_stream = Gitlab::GRPC::TestHelper::RequestEnumerator.new(requests)

          # error is raised only if the stream is consumed
          expect { service.scan_stream(request_stream.each_item).to_a }.to raise_error do |error|
            expect(error).to be_a(::GRPC::InvalidArgument)
            expect(error.details).to eq(described_class::ERROR_MESSAGES[err_msg])
            expect(error.metadata['field']).to eq(err_field)
          end
        end
      end

      context "when exclusion value is blank" do
        requests = [
          ScanRequest.new(
            exclusions: [{ exclusion_type: :EXCLUSION_TYPE_RAW_VALUE, value: "" }] # invalid
          )
        ]

        it_behaves_like "raises argument error when consuming stream", requests, :exclusion_empty_value,
          "exclusion.value"
      end

      context "when scan timeout value is negative" do
        requests = [ScanRequest.new(timeout_secs: -20)]

        it_behaves_like "raises argument error when consuming stream", requests, :invalid_timeout_range,
          "timeout_secs"
      end

      context "when one of the payload streams has invalid input" do
        it "stream request closes with an error on the occurrence of invalid input" do
          allow_any_instance_of(Gitlab::SecretDetection::Core::Scanner).to receive(:secrets_scan)
            .and_return(mock_core_response_without_secrets)

          requests = [
            ScanRequest.new(
              payloads: [ScanRequest::Payload.new(id: '1', data: "valid value")]
            ),
            ScanRequest.new(payload_timeout_secs: -20), # invalid
            ScanRequest.new(payloads: []) # valid but will not be processed
          ]
          request_stream = Gitlab::GRPC::TestHelper::RequestEnumerator.new(requests)

          stream_response = service.scan_stream(request_stream.each_item)

          # runs as expected for the first payload message
          expect(stream_response.next.to_h).to eq(
            {
              results: [],
              status: ScanStatus::STATUS_NOT_FOUND,
              applied_exclusions: []
            }
          )

          # invalid input occurs in the second payload message, raises error and closes the stream
          expect { stream_response.next }.to raise_error do |error|
            expect(error).to be_a(::GRPC::InvalidArgument)
            expect(error.details).to eq(described_class::ERROR_MESSAGES[:invalid_timeout_range])
            expect(error.metadata['field']).to eq('payload_timeout_secs')
          end

          # when invoking to consume third payload message, it raises error about closed stream
          expect { stream_response.next }.to raise_error(::GRPC::Core::CallError) do |error|
            error.message.eql?("Cannot run batch on closed call")
          end
        end
      end

      # New test for error handling in scan_stream
      context "when the stream is closed unexpectedly" do
        it "handles the error gracefully" do
          request = ScanRequest.new(payloads: payloads_with_secrets)
          request_stream = Gitlab::GRPC::TestHelper::RequestEnumerator.new([request])

          # Mock the stream to raise an error when consumed
          allow(request_stream).to receive(:each_item).and_raise(::GRPC::Cancelled.new("Stream cancelled by client"))

          expect { service.scan_stream(request_stream.each_item).to_a }.to raise_error do |error|
            expect(error).to be_a(::GRPC::Cancelled)
            expect(error.message).to include("Stream cancelled by client")
          end
        end
      end
    end
  end

  # These are for coverage and completeness so we test the ScannerService directly instead of via the gRPC client
  context 'when not using the client to communicate' do
    subject(:service) { ScanService.new }

    let(:response) { ScanResponse.new(status: ScanStatus::STATUS_INPUT_ERROR) }
    let(:call_double) { double("Call", deadline: 1, cancelled?: false) } # rubocop:disable Rspec/VerifiedDoubles -- We want to avoid creating gRPC objects

    it 'handles nil requests' do
      expect(service.scan(nil, call_double)).to eq(response)
    end
  end
end
