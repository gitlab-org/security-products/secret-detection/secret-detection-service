# frozen_string_literal: true

require 'time'
require_relative '../../../../spec_helper'

SDGRPC = Gitlab::SecretDetection::GRPC
GRPCStatus = SDGRPC::ScanResponse::Status
SDCore = Gitlab::SecretDetection::Core
SDCoreStatus = SDCore::Status

RSpec.describe Gitlab::SecretDetection::GRPC::Client do
  subject(:client) { described_class.new(host, secure:) }

  let(:secure) { true }
  let(:host) { 'example.com:443' }
  let(:auth_token) { '12345' }
  let(:stub) { instance_double('SDGRPC::Scanner::Stub') }

  let(:payloads) { [SDGRPC::ScanRequest::Payload.new(id: '1', data: 'dummy')] }
  let(:findings) { [SDGRPC::ScanResponse::Finding.new(payload_id: '1', status: 1)] }
  let(:request) { SDGRPC::ScanRequest.new(payloads:) }
  let(:requests) { [request, request] }
  let(:applied_exclusions) do
    [
      SDGRPC::Exclusion.new(
        exclusion_type: SDGRPC::ExclusionType::EXCLUSION_TYPE_RAW_VALUE,
        value: "some value"
      )
    ]
  end

  let(:stub_scan_response) do
    SDGRPC::ScanResponse.new(
      status: GRPCStatus::STATUS_FOUND,
      results: findings,
      applied_exclusions:
    )
  end

  let(:stub_scan_stream_response) do
    [
      SDGRPC::ScanResponse.new(
        status: GRPCStatus::STATUS_NOT_FOUND,
        results: []
      ),
      SDGRPC::ScanResponse.new(
        status: GRPCStatus::STATUS_NOT_FOUND,
        results: []
      )
    ].to_enum
  end

  let(:metadata) { { "x-sd-auth" => auth_token } }

  before do
    allow(SDGRPC::Scanner::Stub).to receive(:new).and_return(stub)
    allow(stub).to receive(:scan).and_return(stub_scan_response)
    allow(stub).to receive(:scan_stream).and_return(stub_scan_stream_response)
  end

  describe "#run_scan" do
    it "sends correct metadata and deadline" do
      before_test_time = Time.now
      client.run_scan(request:, auth_token:)

      expect(stub).to have_received(:scan).with(
        request,
        deadline: satisfy do |deadline|
          diff = (deadline - before_test_time)
          (diff - described_class::REQUEST_TIMEOUT_SECONDS) < 1 # considering buffer of 1 sec
        end,
        metadata:
      )
    end

    it "does not care if result is a SD service response or SD core response" do
      result = client.run_scan(request:, auth_token:)

      expect(result.status).to eq(stub_scan_response.status)
      expect(result.results).to eq(stub_scan_response.results)
      expect(result.applied_exclusions).to eq(stub_scan_response.applied_exclusions)
    end

    context "when an error occurs in the service" do
      it "returns SD core response instead of raising error" do
        allow(stub).to receive(:scan).and_raise(::GRPC::Unauthenticated)
        result = nil
        expect { result = client.run_scan(request:, auth_token:) }.not_to raise_error
        expect(result).to be_instance_of(SDCore::Response)
      end

      it "returns SD response with corresponding Core::Status" do
        [
          [::GRPC::InvalidArgument, SDCore::Status::INPUT_ERROR],
          [::GRPC::Unauthenticated, SDCore::Status::AUTH_ERROR],
          [::GRPC::Unknown, SDCore::Status::SCAN_ERROR],
          [::GRPC::BadStatus, SDCore::Status::SCAN_ERROR]
        ].each do |grpc_error, sd_core_status|
          allow(stub).to receive(:scan).and_raise(grpc_error, "")

          result = nil
          expect { result = client.run_scan(request:, auth_token:) }.not_to raise_error

          expect(result).to be_instance_of(SDCore::Response)
          expect(result&.status).to eq(sd_core_status)
        end
      end
    end
  end

  describe "#run_scan_stream" do
    it "sends correct metadata and deadline" do
      before_test_time = Time.now
      client.run_scan_stream(requests:, auth_token:)

      expect(stub).to have_received(:scan_stream).with(
        requests,
        deadline: satisfy do |deadline|
          diff = (deadline - before_test_time)
          (diff - described_class::REQUEST_TIMEOUT_SECONDS) < 1 # considering buffer of 1 sec
        end,
        metadata:
      )
    end

    it "does not care if each streamed response is a SD core response or GRPC Response" do
      result = client.run_scan_stream(requests:, auth_token:)

      expect(result).to be_instance_of(Array)
      expect(result.length).to eq(requests.length)
      result.each do |msg|
        expect(msg.status).to eq(SDCore::Status::NOT_FOUND)
      end
    end
  end
end
