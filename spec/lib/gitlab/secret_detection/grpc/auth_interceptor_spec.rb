# frozen_string_literal: true

require_relative '../../../../spec_helper'
require_relative '../../../../../server/interceptors/auth_interceptor'

RSpec.describe Gitlab::SecretDetection::GRPC::AuthInterceptor do
  let!(:server_auth_token) { "123456" }
  let!(:interceptor) { described_class.new(server_auth_token:) }
  let(:scanner_svc) { stub_secret_detection_service }
  let(:health_check_svc) { stub_health_check_service }

  before do
    services = [
      Gitlab::SecretDetection::GRPC::ScannerService,
      create_health_check_service
    ]

    start_test_server(services, [interceptor])
  end

  after do
    stop_test_server
  end

  context "when calling RPC methods in Secret Detection Scanner service" do
    context "when no auth token is passed" do
      it "raises unauthenticated error" do
        request = Gitlab::SecretDetection::GRPC::ScanRequest.new(payloads: [])
        metadata = {}
        expect(interceptor).to receive(:authenticated?).and_return(false)
        expect { scanner_svc.scan(request, metadata:) }.to raise_error(GRPC::Unauthenticated)
      end
    end

    context "when an invalid auth token is passed" do
      it "raises unauthenticated error" do
        request = Gitlab::SecretDetection::GRPC::ScanRequest.new(payloads: [])
        metadata = {
          described_class::REQUEST_AUTH_HEADER => "invalid token"
        }
        expect(interceptor).to receive(:authenticated?).and_return(false)
        expect { scanner_svc.scan(request, metadata:) }.to raise_error(GRPC::Unauthenticated)
      end
    end

    context "when a valid auth token is passed" do
      it "passes the authentication and processes the request" do
        request = Gitlab::SecretDetection::GRPC::ScanRequest.new(payloads: [])
        metadata = {
          described_class::REQUEST_AUTH_HEADER => server_auth_token
        }
        expected = Gitlab::SecretDetection::GRPC::ScanResponse.new(results: [],
          status: Gitlab::SecretDetection::GRPC::ScanResponse::Status::STATUS_NOT_FOUND)

        expect(interceptor).to receive(:authenticated?).and_return(true)
        expect(scanner_svc.scan(request, metadata:)).to eq(expected)
      end
    end
  end

  context "when calling RPC methods in other service" do
    context "when no auth token is passed" do
      it "forwards the request without authentication" do
        request = Grpc::Health::V1::HealthCheckRequest.new(service: '')
        metadata = {}
        expected = Grpc::Health::V1::HealthCheckResponse.new(status: :SERVING)

        expect(interceptor).to receive(:authenticated?).and_return(true)
        expect(health_check_svc.check(request, metadata:)).to eq(expected)
      end
    end

    context "when any token is passed" do
      it "forwards the request without authentication" do
        request = Grpc::Health::V1::HealthCheckRequest.new(service: '')
        metadata = {
          described_class::REQUEST_AUTH_HEADER => "random value"
        }
        expected = Grpc::Health::V1::HealthCheckResponse.new(status: :SERVING)

        expect(interceptor).to receive(:authenticated?).and_return(true)
        expect(health_check_svc.check(request, metadata:)).to eq(expected)
      end
    end
  end
end
