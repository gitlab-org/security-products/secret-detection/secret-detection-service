# frozen_string_literal: true

require_relative '../../../../spec_helper'

RSpec.describe Gitlab::SecretDetection::Utils::Masker do
  subject(:masker) { described_class }

  context "for a given raw secret value" do
    let(:raw_secret_value) { "glpat-dummy1235dummy1235" } # not a real glpat token format

    it "returns the masked secret value" do
      expect(masker.mask_secret(raw_secret_value)).to eq("glp*****mmy*****umm*****")
    end

    it "returns the masked secret value for a different mask character" do
      different_mask_char = "X"
      expect(masker.mask_secret(raw_secret_value, mask_char: different_mask_char)).to eq("glpXXXXXmmyXXXXXummXXXXX")
    end

    it "returns the masked secret value for a different visible character count" do
      no_of_visible_chars = 2
      expect(
        masker.mask_secret(raw_secret_value, visible_chars_count: no_of_visible_chars)
      ).to eq("gl*****um*****5d*****23*")
    end

    it "returns the masked secret value for a different mask character count" do
      no_of_mask_chars = 3
      expect(
        masker.mask_secret(raw_secret_value, mask_chars_count: no_of_mask_chars)
      ).to eq("glp***dum***235***my1***")
    end

    it "returns empty for empty secret value" do
      expect(masker.mask_secret("")).to eq("")
    end

    it "returns empty for nil secret value" do
      expect(masker.mask_secret(nil)).to eq('')
    end

    it "returns unmasked when raw secret value length is <= default visible char count" do
      raw_secret = 'a' * described_class::DEFAULT_VISIBLE_CHAR_COUNT

      expect(masker.mask_secret(raw_secret)).to eq(raw_secret)
    end
  end
end
