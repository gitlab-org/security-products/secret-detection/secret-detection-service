# frozen_string_literal: true

require_relative '../../../../spec_helper'

# Name alias to avoid fully-qualified long class name usage
Core = Gitlab::SecretDetection::Core

RSpec.describe Core::Scanner do
  subject(:scanner) { described_class.new(rules:) }

  def create_payload(id:, data:, offset: 0)
    payload = Struct.new('Payload', :id, :data, :offset, keyword_init: true).new(id:, data:, offset:)
    allow(payload).to receive(:size).and_return(data.size)
    payload
  end

  def create_raw_exclusion(value)
    Gitlab::SecretDetection::GRPC::Exclusion.new(value:,
      exclusion_type: Gitlab::SecretDetection::GRPC::ExclusionType::EXCLUSION_TYPE_RAW_VALUE)
  end

  def create_rule_exclusion(value)
    Gitlab::SecretDetection::GRPC::Exclusion.new(value:,
      exclusion_type: Gitlab::SecretDetection::GRPC::ExclusionType::EXCLUSION_TYPE_RULE)
  end

  def create_path_exclusion(value)
    Gitlab::SecretDetection::GRPC::Exclusion.new(value:,
      exclusion_type: Gitlab::SecretDetection::GRPC::ExclusionType::EXCLUSION_TYPE_PATH)
  end

  let(:rules) do
    [
      {
        id: "gitlab_personal_access_token",
        description: "GitLab Personal Access Token",
        regex: '\bglpat-[0-9a-zA-Z_\-]{20}\b',
        tags: ["gitlab_blocking"],
        keywords: ["glpat"]
      },
      {
        id: "gitlab_pipeline_trigger_token",
        title: "GitLab Pipeline Trigger Token Title",
        description: "GitLab Pipeline Trigger Token Description",
        regex: '\bglptt-[0-9a-zA-Z_-]{40}\b',
        tags: ["gitlab_blocking"],
        keywords: ["glptt"]
      },
      {
        id: "gitlab_runner_registration_token",
        description: "GitLab Runner Registration Token",
        regex: '\bGR1348941[0-9a-zA-Z_-]{20}\b',
        tags: ["gitlab_blocking"],
        keywords: ["GR1348941"]
      },
      {
        id: "gitlab_feed_token_v2",
        description: "GitLab Feed Token",
        regex: '\bglft-[0-9a-zA-Z_-]{20}\b',
        tags: ["gitlab_blocking"],
        keywords: ["glft"]
      },
      {
        id: "aws_access_key",
        description: "AWS Access Key",
        regex: '\bAKIA[0-9A-Z]{16}\b',
        tags: ["cloud_provider"],
        keywords: ["AKIA"]
      }
    ]
  end

  it "does not raise an error parsing the toml file" do
    expect { scanner }.not_to raise_error
  end

  context "when it creates RE2 patterns from file data" do
    it "does not raise an error when building patterns" do
      expect { scanner }.not_to raise_error
    end

    it "builds a pattern matcher with default tags" do
      # Test that the default pattern matcher is created with DEFAULT_PATTERN_MATCHER_TAGS
      expect_any_instance_of(RE2::Set).to receive(:add).at_least(4).times.and_call_original
      expect_any_instance_of(RE2::Set).to receive(:compile).and_call_original

      described_class.new(rules:)
    end

    it "raises RulesetCompilationError when pattern compilation fails" do
      # Break the regex pattern to force compilation failure
      invalid_rules = [
        {
          id: "invalid_rule",
          description: "Invalid Rule",
          regex: '[', # Invalid regex pattern
          tags: ["gitlab_blocking"],
          keywords: ["invalid"]
        }
      ]

      # Mock the compile method to return false
      allow_any_instance_of(RE2::Set).to receive(:compile).and_return(false)

      expect do
        described_class.new(rules: invalid_rules)
      end.to raise_error(Core::Ruleset::RulesetCompilationError)
    end

    it "creates a set of unique keywords from rules" do
      scanner = described_class.new(rules:)

      # Access the private keywords set
      keywords = scanner.send(:keywords)

      # Check that keywords from rules are included
      expect(keywords).to include("glpat")
      expect(keywords).to include("glptt")
      expect(keywords).to include("GR1348941")
      expect(keywords).to include("glft")
      expect(keywords).to include("AKIA")

      # Check that keywords set is frozen
      expect(keywords).to be_frozen
    end
  end

  context "when testing keyword filtering" do
    let(:keyword_matcher) { RE2("\\b(glpat|glptt|GR1348941|glft|AKIA)") }

    it "filters payloads with keywords correctly" do
      payloads = [
        create_payload(id: "match1", data: "contains keyword glpat"),
        create_payload(id: "match2", data: "contains keyword AKIA"),
        create_payload(id: "nomatch", data: "no keywords here")
      ]

      # Call the private method
      filtered = scanner.send(:filter_by_keywords, keyword_matcher, payloads)

      # Should return payloads with keywords
      expect(filtered.length).to eq(2)
      expect(filtered.map(&:id)).to contain_exactly("match1", "match2")
    end

    it "returns all payloads when keyword matcher is nil" do
      payloads = [
        create_payload(id: "match1", data: "contains keyword glpat"),
        create_payload(id: "nomatch", data: "no keywords here")
      ]

      # Call the private method with nil matcher
      filtered = scanner.send(:filter_by_keywords, nil, payloads)

      # Should return all payloads
      expect(filtered.length).to eq(2)
      expect(filtered).to eq(payloads)
    end

    it "processes empty payloads array without error" do
      # Call the private method with empty array
      filtered = scanner.send(:filter_by_keywords, keyword_matcher, [])

      # Should return empty array
      expect(filtered).to eq([])
    end

    it "builds a keyword matcher with the appropriate rules" do
      # Only include rules with cloud_provider tag
      tags = ["cloud_provider"]

      # Call build_keyword_matcher with specific tags
      matcher = scanner.send(:build_keyword_matcher, tags:)

      # Should only match AWS keys, not GitLab tokens
      expect(matcher.partial_match?("AKIA1234567890ABCDEF")).to be true
      expect(matcher.partial_match?("glpat-12312312312312312312")).to be false
    end
  end

  context "when matching patterns" do
    context 'when the payload does not contain a secret' do
      let(:payloads) do
        [
          create_payload(id: 1234, data: "no secrets")
        ]
      end

      it "does not match" do
        expected_response = Core::Response.new(status: Core::Status::NOT_FOUND)

        expect(scanner.secrets_scan(payloads)).to eq(expected_response)
      end

      it "attempts to keyword match returning no payloads for further scan" do
        expect(scanner).to receive(:filter_by_keywords)
                             .with(instance_of(RE2::Regexp), payloads)
                             .and_return([])

        scanner.secrets_scan(payloads)
      end

      it "does not attempt to regex match" do
        expect(scanner).not_to receive(:match_rules_bulk)

        scanner.secrets_scan(payloads)
      end
    end

    context "when multiple payloads contains secrets" do
      let(:payloads) do
        [
          create_payload(id: 111, data: "glpat-12312312312312312312"), # gitleaks:allow
          create_payload(id: 222, data: "\n\nglptt-1231231231231231231212312312312312312312"), # gitleaks:allow
          create_payload(id: 333, data: "data with no secret"),
          create_payload(id: 444,
            data: "GR134894112312312312312312312\nglft-12312312312312312312"), # gitleaks:allow
          create_payload(id: 555, data: "data with no secret"),
          create_payload(id: 666, data: "data with no secret"),
          create_payload(id: 777, data: "\nglptt-1231231231231231231212312312312312312312"), # gitleaks:allow
          create_payload(id: 888,
            data: "glpat-12312312312312312312;GR134894112312312312312312312") # gitleaks:allow
        ]
      end

      let(:expected_response) do
        Core::Response.new(
          status: Core::Status::FOUND,
          results:
            [
              Core::Finding.new(
                payloads[0].id,
                Core::Status::FOUND,
                1,
                rules[0][:id],
                rules[0][:description]
              ),
              Core::Finding.new(
                payloads[1].id,
                Core::Status::FOUND,
                3,
                rules[1][:id],
                rules[1][:title]
              ),
              Core::Finding.new(
                payloads[3].id,
                Core::Status::FOUND,
                1,
                rules[2][:id],
                rules[2][:description]
              ),
              Core::Finding.new(
                payloads[3].id,
                Core::Status::FOUND,
                2,
                rules[3][:id],
                rules[3][:description]
              ),
              Core::Finding.new(
                payloads[6].id,
                Core::Status::FOUND,
                2,
                rules[1][:id],
                rules[1][:title]
              ),
              Core::Finding.new(
                payloads[7].id,
                Core::Status::FOUND,
                1,
                rules[0][:id],
                rules[0][:description]
              ),
              Core::Finding.new(
                payloads[7].id,
                Core::Status::FOUND,
                1,
                rules[2][:id],
                rules[2][:description]
              )
            ],
          applied_exclusions: []
        )
      end

      it "attempts to keyword match returning only filtered payloads for further scan" do
        expected = payloads.filter { |b| b.data != "data with no secret" }
        expect(scanner).to receive(:filter_by_keywords)
                             .with(instance_of(RE2::Regexp), payloads)
                             .and_return(expected)
        scanner.secrets_scan(payloads)
      end

      it "matches multiple rules" do
        expect(scanner.secrets_scan(payloads)).to eq(expected_response)
      end

      context "in subprocess" do
        let(:dummy_lines) do
          10_000
        end

        let(:large_payloads) do
          dummy_data = "\nrandom data" * dummy_lines
          [
            create_payload(id: 111, data: "glpat-12312312312312312312#{dummy_data}"),
            create_payload(id: 222, data: "\n\nglptt-1231231231231231231212312312312312312312#{dummy_data}"),
            create_payload(id: 333, data: "data with no secret#{dummy_data}"),
            create_payload(id: 444,
              data: "GR134894112312312312312312312\nglft-12312312312312312312#{dummy_data}"),
            create_payload(id: 555, data: "data with no secret#{dummy_data}"),
            create_payload(id: 666, data: "data with no secret#{dummy_data}"),
            create_payload(id: 777, data: "#{dummy_data}\nglptt-1231231231231231231212312312312312312312")
          ]
        end

        it "matches multiple rules" do
          expect(scanner.secrets_scan(payloads, subprocess: true)).to eq(expected_response)
        end
      end

      context "when given payload offset" do
        let(:secret) { 'glpat-12312312312312312312' } # gitleaks:allow

        let(:payloads_w_offset) do
          [
            create_payload(id: 111, data: "#{secret}\nsecond line\nthird line", offset: 10),
            create_payload(id: 222, data: "first line\n#{secret}\nthird line", offset: 20),
            create_payload(id: 333, data: "first line\nsecond line\n#{secret}", offset: 30)
          ]
        end

        let(:payload1_secret_index) { 0 }
        let(:payload2_secret_index) { 1 }
        let(:payload3_secret_index) { 2 }

        # we are subtracting -1 since the offset is the line number of the first line in the payload data
        let(:expected_line_numbers) do
          [
            payloads_w_offset[0].offset + payload1_secret_index, # computed line number = 10
            payloads_w_offset[1].offset + payload2_secret_index, # computed line number = 21
            payloads_w_offset[2].offset + payload3_secret_index # computed line number = 32
          ]
        end

        it "returns findings with absolute line number" do
          expected_payloads = payloads_w_offset.map.with_index do |payload, index|
            Core::Finding.new(
              payload.id,
              Core::Status::FOUND,
              expected_line_numbers[index],
              rules[0][:id],
              rules[0][:description]
            )
          end

          expected_response = Core::Response.new(
            status: Core::Status::FOUND,
            results: expected_payloads,
            applied_exclusions: []
          )

          expect(scanner.secrets_scan(payloads_w_offset)).to eq(expected_response)
        end
      end
    end

    context "when testing find_secrets_in_payload functionality" do
      it "finds secrets in payload data correctly" do
        payload = create_payload(
          id: "test",
          data: "Line 1\nLine 2 with secret glpat-12312312312312312312\nLine 3" # gitleaks:allow
        )

        pattern_matcher, _ = scanner.send(:build_pattern_matcher, tags: Core::Scanner::DEFAULT_PATTERN_MATCHER_TAGS)

        # Call the private method
        findings, applied_exclusions = scanner.send(:find_secrets_in_payload,
          payload:,
          pattern_matcher:
        )

        # Should find the secret on line 2
        expect(findings.length).to eq(1)
        expect(findings.first.line_number).to eq(2)
        expect(findings.first.type).to eq("gitlab_personal_access_token")
        expect(applied_exclusions).to be_empty
      end

      it "respects payload offset when computing line numbers" do
        # Payload with offset 10
        payload = create_payload(
          id: "test",
          data: "Line 1\nLine 2 with secret glpat-12312312312312312312", # gitleaks:allow
          offset: 10
        )

        pattern_matcher, _ = scanner.send(:build_pattern_matcher, tags: Core::Scanner::DEFAULT_PATTERN_MATCHER_TAGS)

        # Call the private method
        findings, _ = scanner.send(:find_secrets_in_payload,
          payload:,
          pattern_matcher:
        )

        # Line number should be offset + line index
        # Secret is on the second line (index 1), so line number should be 10 + 1 = 11
        expect(findings.first.line_number).to eq(11)
      end

      it "handles errors during processing" do
        payload = create_payload(id: "test", data: "Test data")

        # Mock pattern_matcher to raise an error during match
        pattern_matcher = instance_double(RE2::Set)
        allow(pattern_matcher).to receive(:match).and_raise(StandardError, "Test error")

        # Call the private method
        findings, _ = scanner.send(:find_secrets_in_payload,
          payload:,
          pattern_matcher:
        )

        # Should return a finding with SCAN_ERROR status
        expect(findings.length).to eq(1)
        expect(findings.first.status).to eq(Core::Status::SCAN_ERROR)
      end
    end

    context "when testing subprocess functionality" do
      it "groups payloads correctly by chunk size" do
        # Call the private method directly
        groups = scanner.send(:group_by_chunk_size, [100, 200, 300, Core::Scanner::MIN_CHUNK_SIZE_PER_PROC_BYTES + 1])

        # Small payloads should be grouped together
        expect(groups.size).to be >= 1

        # The large payload should be in its own group
        large_payload_index = 3
        expect(groups.any? { |g| g.include?(large_payload_index) }).to be true
      end

      it "properly distributes payloads in subprocess mode" do
        small_payload = create_payload(id: "small",
          data: "small data with secret glpat-12312312312312312312") # gitleaks:allow

        # Execute in subprocess mode
        allow(Parallel).to receive(:flat_map).and_call_original

        result = scanner.secrets_scan([small_payload], subprocess: true)

        # Should have tried to use Parallel for processing
        expect(Parallel).to have_received(:flat_map)

        # Should still detect the secret
        expect(result.status).to eq(Core::Status::FOUND)
      end
    end

    context "when configured with time out" do
      let(:each_payload_timeout_secs) { 0.001 } # 1 millisec to intentionally timeout large payload

      let(:large_data) do
        ("large data with a secret glpat-12312312312312312312\n" * 10_000_000).freeze # gitleaks:allow
      end

      let(:payloads) do
        [
          create_payload(id: 111, data: "GR134894112312312312312312312"), # gitleaks:allow
          create_payload(id: 333, data: "data with no secret"),
          create_payload(id: 333, data: large_data)
        ]
      end

      let(:all_large_payloads) do
        [
          create_payload(id: 111, data: large_data),
          create_payload(id: 222, data: large_data),
          create_payload(id: 333, data: large_data)
        ]
      end

      it "whole secret detection scan operation times out" do
        scan_timeout_secs = 0.000_001 # 1 micro-sec to intentionally timeout large payload

        expected_response = Core::Response.new(
          status: Core::Status::SCAN_TIMEOUT
        )

        response = scanner.secrets_scan(payloads, timeout: scan_timeout_secs)
        expect(response).to eq(expected_response)
      end

      it "one of the payloads times out while others continue to get scanned" do
        given_payloads = [payloads[0], all_large_payloads[0]] # one small payload and one intentionally large payload

        expected_response = Core::Response.new(
          status: Core::Status::FOUND_WITH_ERRORS,
          results:
            [
              Core::Finding.new(
                payloads[0].id,
                Core::Status::FOUND,
                1,
                rules[2][:id],
                rules[2][:description]
              ),
              Core::Finding.new(
                all_large_payloads[0].id,
                Core::Status::PAYLOAD_TIMEOUT
              )
            ],
          applied_exclusions: []
        )

        scan_result = scanner.secrets_scan(
          given_payloads,
          payload_timeout: each_payload_timeout_secs
        )

        expect(scan_result.to_h).to eq(expected_response.to_h)
      end

      it "all the payloads time out" do
        # scan status changes to SCAN_TIMEOUT when *all* the payloads time out
        expected_scan_status = Core::Status::SCAN_TIMEOUT

        expected_response = Core::Response.new(
          status: expected_scan_status,
          results:
            [
              Core::Finding.new(
                all_large_payloads[0].id,
                Core::Status::PAYLOAD_TIMEOUT
              ),
              Core::Finding.new(
                all_large_payloads[1].id,
                Core::Status::PAYLOAD_TIMEOUT
              ),
              Core::Finding.new(
                all_large_payloads[2].id,
                Core::Status::PAYLOAD_TIMEOUT
              )
            ],
          applied_exclusions: []
        )

        expect(scanner.secrets_scan(all_large_payloads,
          payload_timeout: each_payload_timeout_secs)).to eq(expected_response)
      end
    end

    context "when configured with rule tags" do
      let(:payloads) do
        [
          create_payload(id: 111, data: "data with no secret"),
          create_payload(id: 555, data: "glpat-12312312312312312312"), # gitleaks:allow,
          create_payload(id: 666, data: "AKIA1234567890ABCDEF") # gitleaks:allow
        ]
      end

      it "reuses the default pattern matcher for default tags" do
        default_tags = Core::Scanner::DEFAULT_PATTERN_MATCHER_TAGS

        expect(scanner).not_to receive(:build_pattern_matcher).with(
          rules:,
          tags: default_tags)
        scanner.secrets_scan(payloads)
      end

      it "builds a new pattern matcher for non-default tags" do
        non_default_tags = %w[gitlab_blocking gitlab]
        expect(scanner).to receive(:build_pattern_matcher).with(
          tags: non_default_tags).and_call_original
        scanner.secrets_scan(payloads, tags: non_default_tags)
      end

      it "filters rules by tag properly" do
        # Use cloud_provider tag to match only the AWS key
        cloud_provider_tags = %w[cloud_provider]

        # First build a custom keyword matcher for filtering
        expect(scanner).to receive(:build_keyword_matcher).with(tags: cloud_provider_tags).and_call_original

        # Then build a pattern matcher with the same tag
        expect(scanner).to receive(:build_pattern_matcher).with(tags: cloud_provider_tags).and_call_original

        result = scanner.secrets_scan(payloads, tags: cloud_provider_tags)

        # Should only find the AWS key, not the GitLab tokens
        expect(result.results.length).to eq(1)
        expect(result.results.first.type).to eq("aws_access_key")
      end
    end

    context "when using exclusions" do
      let(:payloads) do
        [
          create_payload(id: 111, data: "data with no secret"),
          create_payload(id: 222, data: "GR134894145645645645645645645"), # gitleaks:allow
          create_payload(id: 333, data: "GR134894145645645645645645789"), # gitleaks:allow
          create_payload(id: 444, data: "GR134894112312312312312312312"), # gitleaks:allow
          create_payload(id: 555, data: "glpat-12312312312312312312"), # gitleaks:allow,
          create_payload(id: 666,
            data: "test data\nglptt-1231231231231231231212312312312312312312\nline contd") # gitleaks:allow
        ]
      end

      context "when raw value exclusions are given" do
        let(:status_found) { Core::Status::FOUND }
        let(:expected_response) do
          Core::Response.new(
            status: status_found,
            results:
              [
                Core::Finding.new(
                  payloads[1].id,
                  status_found, 1, rules[2][:id], rules[2][:description]
                ),
                Core::Finding.new(
                  payloads[2].id,
                  status_found, 1, rules[2][:id], rules[2][:description]
                ),
                Core::Finding.new(
                  payloads[5].id,
                  status_found, 2, rules[1][:id], rules[1][:title]
                )
              ],
            applied_exclusions: exclude_values[:raw_value][..1]
          )
        end

        let(:exclude_values) do
          {
            raw_value:
              [
                create_raw_exclusion("GR134894112312312312312312312"), # payloads[3]
                create_raw_exclusion("glpat-12312312312312312312"), # payloads[4]
                create_raw_exclusion("glpat-12312312312312312345") # not in the given payloads
              ]
          }
        end

        it "removes them from the input before matching it against re2 matcher" do
          valid_lines = [payloads[1].data, payloads[2].data, *payloads[5].data.split("\n")]

          expect_any_instance_of(RE2::Set).to receive(:match)
                                                .with(satisfy { |l| valid_lines.include?(l) }, exception: false)
                                                .exactly(valid_lines.size).times
                                                .and_call_original

          scanner.secrets_scan(payloads, exclusions: exclude_values)
        end

        it "excludes values from being detected" do
          expect(scanner.secrets_scan(payloads, exclusions: exclude_values)).to eq(expected_response)
        end

        it "tracks which exclusions were applied" do
          result = scanner.secrets_scan(payloads, exclusions: exclude_values)

          # Check that only exclusions that matched content were recorded as applied
          expect(result.applied_exclusions.length).to eq(2)
          applied_values = result.applied_exclusions.map(&:value)
          expect(applied_values).to include("GR134894112312312312312312312")
          expect(applied_values).to include("glpat-12312312312312312312")
          expect(applied_values).not_to include("glpat-12312312312312312345")
        end
      end

      context "when rule exclusions are given" do
        let(:rule_exclusions) do
          {
            rule:
              [
                create_rule_exclusion("gitlab_runner_registration_token"), # payloads ids: 222, 333, 444
                create_rule_exclusion("gitlab_personal_access_token") # payload ids: 555
              ]
          }
        end

        let(:status_found) { Core::Status::FOUND }

        let(:expected_response) do
          Core::Response.new(
            status: status_found,
            results:
              [
                Core::Finding.new(
                  payloads[5].id,
                  status_found,
                  2,
                  rules[1][:id],
                  rules[1][:title]
                )
              ],
            applied_exclusions: rule_exclusions[:rule]
          )
        end

        it "filters out from the detected findings" do
          # pattern matcher will not be rebuilt using rules excluding from the
          # given ones, instead we remove the findings for excluded rules from
          # the scan result later
          expect(instance_of(RE2::Set)).not_to receive(:compile)

          expect(scanner.secrets_scan(payloads, exclusions: rule_exclusions)).to eq(expected_response)
        end

        it "applies rule exclusions correctly" do
          result = scanner.secrets_scan(payloads, exclusions: rule_exclusions)

          # No findings of excluded rule types should be present
          expect(result.results.map(&:type)).not_to include("gitlab_runner_registration_token")
          expect(result.results.map(&:type)).not_to include("gitlab_personal_access_token")

          # But other rule types should still be detected
          expect(result.results.map(&:type)).to include("gitlab_pipeline_trigger_token")
        end
      end

      context "when path exclusions are given" do
        let(:path_exclusions) do
          {
            path: [
              create_path_exclusion("test/path/*")
            ]
          }
        end

        it "passes path exclusions to the scanner" do
          # Check that path exclusions are properly passed to scanner
          # Even though we don't actually filter by path in the test
          expect(scanner).to receive(:find_secrets_in_payload)
            .with(hash_including(exclusions: hash_including(path: path_exclusions[:path])))
            .at_least(:once)
            .and_call_original

          scanner.secrets_scan(payloads, exclusions: path_exclusions)
        end
      end

      context "when mixed exclusions are provided" do
        let(:mixed_exclusions) do
          {
            raw_value: [create_raw_exclusion("glpat-12312312312312312312")],
            rule: [create_rule_exclusion("gitlab_runner_registration_token")],
            path: [create_path_exclusion("test/path/*")]
          }
        end

        it "processes all exclusion types" do
          # All types should be passed to find_secrets_in_payload
          expect(scanner).to receive(:find_secrets_in_payload)
            .with(
              hash_including(
                exclusions: hash_including(
                  raw_value: mixed_exclusions[:raw_value],
                  rule: mixed_exclusions[:rule],
                  path: mixed_exclusions[:path]
                )
              )
            )
            .at_least(:once)
            .and_call_original

          result = scanner.secrets_scan(payloads, exclusions: mixed_exclusions)

          # Results should reflect both raw value and rule exclusions
          expect(result.results.map(&:type)).not_to include("gitlab_runner_registration_token")

          # Applied exclusions should include both types that were actually used
          applied_types = result.applied_exclusions.map(&:exclusion_type).uniq
          expect(applied_types).to include(:EXCLUSION_TYPE_RAW_VALUE)
          expect(applied_types).to include(:EXCLUSION_TYPE_RULE)
        end
      end
    end

    context "with special payload data" do
      it "handles payloads with non-ASCII characters" do
        # Create payload with emoji and non-ASCII characters
        payload_with_unicode = create_payload(
          id: "unicode",
          data: "Unicode 😀 characters with secret glpat-12312312312312312312" # gitleaks:allow
        )

        result = scanner.secrets_scan([payload_with_unicode])

        # Should detect the secret despite the non-ASCII characters
        expect(result.status).to eq(Core::Status::FOUND)
        expect(result.results.first.type).to eq("gitlab_personal_access_token")
      end

      it "handles multi-line code blocks" do
        # Code block with secret in a comment
        code_block = <<~CODE
          def test_method():
              # This is a fake token: glpat-12312312312312312312
              return True
        CODE

        payload = create_payload(id: "code_block", data: code_block)
        result = scanner.secrets_scan([payload])

        # Should find the secret in the comment
        expect(result.status).to eq(Core::Status::FOUND)
        expect(result.results.first.line_number).to eq(2) # On line 2
      end

      it "handles very large payloads" do
        # Create a payload with 100KB of data and a secret at the end
        large_data = "#{'x' * 100_000} \nglpat-12312312312312312312" # gitleaks:allow
        large_payload = create_payload(id: "large", data: large_data)

        # Should be able to scan large payload without timeouts with default settings
        result = scanner.secrets_scan([large_payload])

        expect(result.status).to eq(Core::Status::FOUND)
        expect(result.results.first.type).to eq("gitlab_personal_access_token")
      end
    end

    context "with edge cases and error conditions" do
      it "handles empty payloads gracefully" do
        empty_payloads = []
        result = scanner.secrets_scan(empty_payloads)

        expect(result.status).to eq(Core::Status::NOT_FOUND)
      end

      it "handles nil payloads gracefully" do
        result = scanner.secrets_scan(nil)

        expect(result.status).to eq(Core::Status::INPUT_ERROR)
      end

      it "handles payloads with missing required methods" do
        invalid_payload = Object.new # Has neither id nor data method

        result = scanner.secrets_scan([invalid_payload])

        expect(result.status).to eq(Core::Status::INPUT_ERROR)
      end

      it "handles zero timeout values by using defaults" do
        # When zero is provided for timeouts, defaults should be used
        payload = create_payload(id: "test", data: "glpat-12312312312312312312") # gitleaks:allow

        # Should use default timeout values
        expect(scanner).to receive(:run_scan).with(
          hash_including(
            payload_timeout: Core::Scanner::DEFAULT_PAYLOAD_TIMEOUT_SECS
          )
        ).and_call_original

        scanner.secrets_scan([payload], timeout: 0, payload_timeout: 0)
      end

      it "uses subprocess mode when requested" do
        payload = create_payload(id: "test", data: "glpat-12312312312312312312") # gitleaks:allow

        expect(scanner).to receive(:run_scan_within_subprocess).and_call_original

        scanner.secrets_scan([payload], subprocess: true)
      end

      it "groups payloads by size in subprocess mode" do
        # Create payloads of different sizes
        payloads = [
          create_payload(id: "small1", data: "x" * 100),
          create_payload(id: "small2", data: "y" * 200),
          create_payload(id: "large", data: "z" * (Core::Scanner::MIN_CHUNK_SIZE_PER_PROC_BYTES + 1))
        ]

        allow(scanner).to receive(:filter_by_keywords).and_return(payloads)
        # Spy on the group_by_chunk_size method
        allow(scanner).to receive(:group_by_chunk_size).and_call_original

        scanner.secrets_scan(payloads, subprocess: true)

        # Should have called the grouping method with the payload sizes
        expect(scanner).to have_received(:group_by_chunk_size).with(
          [100, 200, Core::Scanner::MIN_CHUNK_SIZE_PER_PROC_BYTES + 1]
        )
      end

      #      TODO: I'm not sure if we should test this and if so, what the proper behavior should be
      #      it "returns SCAN_ERROR status when an unexpected error occurs" do
      #        payload = create_payload(id: "test", data: "glpat-12312312312312312312") # gitleaks:allow
      #
      #        # Force an error during find_secrets_in_payload
      #        allow(scanner).to receive(:find_secrets_in_payload).and_raise(StandardError, "Unexpected error")
      #
      #        result = scanner.secrets_scan([payload])
      #
      #        # Should handle the error and return SCAN_ERROR status
      #        expect(result.status).to eq(Core::Status::FOUND_WITH_ERRORS)
      #        expect(result.results.first.status).to eq(Core::Status::SCAN_ERROR)
      #      end

      it "handles payloads with just id and not data" do
        # Create a payload that has id but data method returns nil
        payload = double("Payload") # rubocop:disable  RSpec/VerifiedDoubles -- just need a simple double
        allow(payload).to receive(:id).and_return("test_id")
        allow(payload).to receive(:data).and_return(nil)

        result = scanner.secrets_scan([payload])

        # Should not raise an error, but handle it properly
        expect(result.status).to eq(Core::Status::INPUT_ERROR)
      end

      it "formats exclusions properly for logging" do
        # Test the format_exclusions_arr method
        exclusions = [
          create_raw_exclusion("secret-value"),
          create_rule_exclusion("gitlab_personal_access_token"),
          create_path_exclusion("path/to/file")
        ]

        # Call the private method
        formatted = scanner.send(:format_exclusions_arr, exclusions)

        expect(formatted).to include(a_string_matching(/rules=gitlab_personal_access_token/))
        expect(formatted).to include(a_string_matching(/raw_values=/)) # Value should be masked
        expect(formatted).to include(a_string_matching(%r{paths=path/to/file}))
      end
    end

    context "with validation of inputs" do
      it "validates timeout range" do
        payload = create_payload(id: "test", data: "Test data")

        allow(scanner).to receive(:filter_by_keywords).and_return([payload])

        # Negative timeout should use default
        expect(scanner).not_to receive(:run_scan_within_subprocess)
        expect(scanner).to receive(:run_scan).with(
          hash_including(payload_timeout: Core::Scanner::DEFAULT_PAYLOAD_TIMEOUT_SECS)
        ).and_call_original

        scanner.secrets_scan([payload], payload_timeout: -5)
      end

      it "defaults to normal scan when subprocess is false" do
        payload = create_payload(id: "test", data: "Test data")

        allow(scanner).to receive(:filter_by_keywords).and_return([payload])
        expect(scanner).not_to receive(:run_scan_within_subprocess)
        expect(scanner).to receive(:run_scan).and_call_original

        scanner.secrets_scan([payload], subprocess: false)
      end

      it "defaults to DEFAULT_PATTERN_MATCHER_TAGS when tags is empty" do
        payload = create_payload(id: "test", data: "Test data")

        # Should use the default pattern matcher
        expect(scanner).not_to receive(:build_pattern_matcher)

        scanner.secrets_scan([payload], tags: [])
      end

      it "validates the payload has both id and data methods" do
        # Test that validate_scan_input properly checks for id and data
        valid_payload = create_payload(id: "valid", data: "Valid data")
        invalid_payload = double("InvalidPayload") # rubocop:disable  RSpec/VerifiedDoubles -- just not meant to double a specific object
        allow(invalid_payload).to receive(:id).and_return("test")
        # No data method

        # Call private method directly
        expect(scanner.send(:validate_scan_input, [valid_payload])).to be true
        expect(scanner.send(:validate_scan_input, [invalid_payload])).to be false
      end

      it "respects valid timeout values" do
        payload = create_payload(id: "test", data: "Test data")
        custom_timeout = 120
        custom_payload_timeout = 30

        expect(Timeout).to receive(:timeout).with(custom_timeout).and_call_original

        scanner.secrets_scan(
          [payload],
          timeout: custom_timeout,
          payload_timeout: custom_payload_timeout
        )
      end

      it "properly validates and processes empty exclusions" do
        payload = create_payload(id: "test", data: "Test data with secret: glpat-12312312312312312312") # gitleaks:allow

        # Empty exclusions should be treated as no exclusions
        result = scanner.secrets_scan([payload], exclusions: {})

        # Should still detect the secret
        expect(result.status).to eq(Core::Status::FOUND)
        expect(result.results.first.type).to eq("gitlab_personal_access_token")
      end
    end
  end
end
