# frozen_string_literal: true

require_relative '../../../../spec_helper'

# Name alias to avoid fully-qualified long class name usage
Core = Gitlab::SecretDetection::Core

RSpec.describe Core::Ruleset do
  subject(:ruleset) { described_class.new(path: test_ruleset_path, logger: test_logger) }

  let(:test_logger) { instance_double(Logger, info: nil, error: nil) }
  let(:test_ruleset_path) { '/mock/path/test_ruleset.toml' }
  let(:standard_ruleset_path) { described_class::RULESET_FILE_PATH }
  let(:valid_toml_content) do
    <<~TOML
      # Version: 1.2.3
      [[rules]]
      id = "test_rule_1"
      description = "Test Rule 1"
      regex = '\\\\btest-[0-9a-zA-Z_-]{10}\\\\b'
      tags = ["test", "gitlab_blocking"]
      keywords = ["test"]

      [[rules]]
      id = "test_rule_2"
      description = "Test Rule 2"
      regex = '\\\\bsecret-[0-9a-zA-Z_-]{20}\\\\b'
      tags = ["gitlab_blocking"]
      keywords = ["secret"]
    TOML
  end

  let(:valid_toml_rule_structure) do
    <<~TOML
      # Version: 1.2.3
      [[rules]]
      id = "test_rule_with_all_fields"
      title = "Test Rule Title"
      description = "Test Rule Description"
      regex = '\\\\btest-[a-z]+\\\\b'
      tags = ["test", "gitlab_blocking"]
      keywords = ["test"]

      [[rules]]
      id = "test_rule_minimal"
      description = "Minimal Test Rule"
      regex = '\\\\bminimal-[0-9]+\\\\b'
    TOML
  end

  describe '#initialize' do
    it 'initializes with default path when not specified' do
      ruleset_with_default = described_class.new(logger: test_logger)
      expect(ruleset_with_default.instance_variable_get(:@path))
        .to eq(standard_ruleset_path)
    end

    it 'initializes with custom path when specified' do
      expect(ruleset.instance_variable_get(:@path)).to eq(test_ruleset_path)
    end

    it 'initializes with the provided logger' do
      expect(ruleset.instance_variable_get(:@logger)).to eq(test_logger)
    end
  end

  describe '#rules' do
    before do
      # Mock TomlRB.load_file to return parsed data
      allow(TomlRB).to receive(:load_file)
        .with(test_ruleset_path, symbolize_keys: true)
        .and_return(
          {
            rules: [
              {
                id: "test_rule_1",
                description: "Test Rule 1",
                regex: "\\btest-[0-9a-zA-Z_-]{10}\\b",
                tags: %w[test gitlab_blocking],
                keywords: %w[test]
              },
              {
                id: "test_rule_2",
                description: "Test Rule 2",
                regex: "\\bsecret-[0-9a-zA-Z_-]{20}\\b",
                tags: %w[gitlab_blocking],
                keywords: %w[secret]
              }
            ]
          }.freeze
        )

      # Mock the version extraction
      allow(ruleset).to receive(:extract_ruleset_version).and_return("1.2.3")
    end

    it 'parses the ruleset file correctly' do
      rules = ruleset.rules
      expect(rules).to be_an(Array)
      expect(rules.size).to eq(2)
      expect(rules[0][:id]).to eq('test_rule_1')
      expect(rules[1][:id]).to eq('test_rule_2')
    end

    it 'logs info about parsing the ruleset file' do
      expect(test_logger).to receive(:info).with(
        message: "Parsing local ruleset file",
        ruleset_path: anything
      )
      ruleset.rules
    end

    it 'only parses the ruleset once unless forced' do
      ruleset.rules
      ruleset.rules
      # Logger should only receive info once for parsing
      expect(test_logger).to have_received(:info).with(
        message: "Parsing local ruleset file",
        ruleset_path: anything
      ).once
    end

    it 'parses the ruleset again when force_fetch is true' do
      ruleset.rules
      ruleset.rules(force_fetch: true)
      # Logger should receive info twice for parsing
      expect(test_logger).to have_received(:info).with(
        message: "Parsing local ruleset file",
        ruleset_path: anything
      ).twice
    end

    context 'when ruleset file cannot be parsed' do
      before do
        allow(TomlRB).to receive(:load_file)
          .with(test_ruleset_path, symbolize_keys: true)
          .and_raise(TomlRB::ParseError, 'Invalid TOML')
      end

      it 'logs an error and raises RulesetParseError' do
        expect(test_logger).to receive(:error).with(
          hash_including(message: /Failed to parse local secret detection ruleset/)
        )
        expect { ruleset.rules }.to raise_error(Core::Ruleset::RulesetParseError)
      end
    end

    context 'when ruleset file does not exist' do
      before do
        allow(TomlRB).to receive(:load_file)
          .with(test_ruleset_path, symbolize_keys: true)
          .and_raise(Errno::ENOENT, 'No such file or directory')
      end

      it 'logs an error and raises RulesetParseError' do
        expect(test_logger).to receive(:error).with(
          hash_including(message: /Failed to parse local secret detection ruleset/)
        )
        expect { ruleset.rules }.to raise_error(Core::Ruleset::RulesetParseError)
      end
    end
  end

  describe '#extract_ruleset_version' do
    let(:ruleset_path) { described_class::RULESET_FILE_PATH }

    before do
      # Mock File.readable?
      allow(File).to receive(:readable?).with(ruleset_path).and_return(true)

      # Mock File.open to return a file handle that yields a line with the version
      mock_file = StringIO.new("# Version: 1.2.3\n")
      allow(File).to receive(:open).with(ruleset_path).and_yield(mock_file)
    end

    it 'extracts the version from the first line comment' do
      expect(ruleset.extract_ruleset_version).to eq('1.2.3')
    end

    context 'when the file does not have a version comment' do
      before do
        mock_file = StringIO.new("[[rules]]\nid = \"test_rule\"\n")
        allow(File).to receive(:open).with(ruleset_path).and_yield(mock_file)
      end

      it 'returns nil' do
        expect(ruleset.extract_ruleset_version).to be_nil
      end
    end

    context 'when the file cannot be read' do
      before do
        allow(File).to receive(:readable?).with(ruleset_path).and_return(false)
      end

      it 'returns nil' do
        expect(ruleset.extract_ruleset_version).to be_nil
      end
    end

    context 'when an error occurs while reading the file' do
      before do
        allow(File).to receive(:open)
          .with(ruleset_path)
          .and_raise(StandardError, 'Test error')
        allow(test_logger).to receive(:error)
      end

      it 'logs an error and returns nil' do
        expect(ruleset.extract_ruleset_version).to be_nil
        expect(test_logger).to have_received(:error).with(
          hash_including(
            message: /Failed to extract Secret Detection Ruleset version/
          )
        )
      end
    end
  end

  describe 'rule structure' do
    before do
      # Create a frozen result that includes frozen hash elements
      rules_array = [
        {
          id: "test_rule_with_all_fields",
          title: "Test Rule Title",
          description: "Test Rule Description",
          regex: "\\btest-[a-z]+\\b",
          tags: %w[test gitlab_blocking],
          keywords: %w[test]
        }.freeze,
        {
          id: "test_rule_minimal",
          description: "Minimal Test Rule",
          regex: "\\bminimal-[0-9]+\\b"
        }.freeze
      ].freeze

      # Mock TomlRB.load_file to return a complex rule structure
      allow(TomlRB).to receive(:load_file)
        .with(test_ruleset_path, symbolize_keys: true)
        .and_return(
          {
            rules: rules_array
          }.freeze
        )

      allow(ruleset).to receive(:extract_ruleset_version).and_return("1.2.3")
    end

    it 'parses rules with all fields correctly' do
      rules = ruleset.rules
      expect(rules[0]).to include(
        id: "test_rule_with_all_fields",
        title: "Test Rule Title",
        description: "Test Rule Description",
        regex: "\\btest-[a-z]+\\b",
        tags: %w[test gitlab_blocking],
        keywords: %w[test]
      )
    end

    it 'parses rules with minimal fields correctly' do
      rules = ruleset.rules
      expect(rules[1]).to include(
        id: "test_rule_minimal",
        description: "Minimal Test Rule",
        regex: "\\bminimal-[0-9]+\\b"
      )
      expect(rules[1][:tags]).to be_nil
      expect(rules[1][:keywords]).to be_nil
      expect(rules[1][:title]).to be_nil
    end

    it 'freezes the parsed rules' do
      rules = ruleset.rules
      expect(rules).to be_frozen
      expect(rules[0]).to be_frozen
    end
  end
end
