# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Interface Equality between Core and GRPC', :integration do
  # This spec ensures that the interfaces between the Core classes and their
  # corresponding GRPC classes are in sync, which is essential for proper
  # communication between the service layers.

  describe 'Status values equality' do
    it 'has matching status constants between Core::Status and GRPC::ScanResponse::Status', :aggregate_failures do
      # The enum values in the proto file should match the constants in Core::Status
      core_status = Gitlab::SecretDetection::Core::Status
      grpc_status = Gitlab::SecretDetection::GRPC::ScanResponse::Status

      # Get all status-related constants (exclude non-status constants like values_map)
      core_constants = core_status.constants.select { |c| c.to_s.upcase == c.to_s }
      grpc_constants = grpc_status.constants.select { |c| c.to_s.start_with?('STATUS_') }

      # Check that they have the same number of status constants
      expect(core_constants.count).to eq(grpc_constants.count),
        "Core::Status has #{core_constants.count} constants (#{core_constants}), " \
          "but GRPC::ScanResponse::Status has #{grpc_constants.count} constants (#{grpc_constants})"

      # Test all status constants defined in Core::Status
      expect(core_status::UNSPECIFIED).to eq(grpc_status::STATUS_UNSPECIFIED)
      expect(core_status::FOUND).to eq(grpc_status::STATUS_FOUND)
      expect(core_status::FOUND_WITH_ERRORS).to eq(grpc_status::STATUS_FOUND_WITH_ERRORS)
      expect(core_status::SCAN_TIMEOUT).to eq(grpc_status::STATUS_SCAN_TIMEOUT)
      expect(core_status::PAYLOAD_TIMEOUT).to eq(grpc_status::STATUS_PAYLOAD_TIMEOUT)
      expect(core_status::SCAN_ERROR).to eq(grpc_status::STATUS_SCAN_ERROR)
      expect(core_status::INPUT_ERROR).to eq(grpc_status::STATUS_INPUT_ERROR)
      expect(core_status::NOT_FOUND).to eq(grpc_status::STATUS_NOT_FOUND)
      expect(core_status::AUTH_ERROR).to eq(grpc_status::STATUS_AUTH_ERROR)
    end
  end

  describe 'Finding equality' do
    let(:expected_grpc_finding_fields) do
      %w[payload_id status type description line_number]
    end

    let(:expected_core_finding_fields) do
      [:to_h, :status, :description, :state, :payload_id, :line_number, :type, :==]
    end

    # Intended to catch situations where the GRPC Finding is updated but not the Core::Finding in the monolith
    it 'GRPC::ScanResponse::Finding has only the expected protobuf fields' do
      fields = Gitlab::SecretDetection::GRPC::ScanResponse::Finding.descriptor.entries
      expect(fields.map(&:name)).to match_array(expected_grpc_finding_fields)
    end

    it 'Core::Finding has only the expected methods' do
      expect(Gitlab::SecretDetection::Core::Finding.instance_methods(false)).to match_array(
        expected_core_finding_fields
      )
    end

    it 'has matching field structure between Core::Finding and GRPC::ScanResponse::Finding', :aggregate_failures do
      # Create instances of both classes with the same values
      payload_id = 'test_payload'
      status = Gitlab::SecretDetection::Core::Status::FOUND
      line_number = 42
      type = 'test_type'
      description = 'test_description'

      core_finding = Gitlab::SecretDetection::Core::Finding.new(
        payload_id, status, line_number, type, description
      )

      grpc_finding = Gitlab::SecretDetection::GRPC::ScanResponse::Finding.new(
        payload_id:,
        status:,
        line_number:,
        type:,
        description:
      )

      # Compare the hash representation to ensure field equality
      core_hash = core_finding.to_h
      grpc_hash = grpc_finding.to_h

      # Convert any symbol keys to strings for comparison
      normalized_core_hash = normalize_hash_keys(core_hash)
      normalized_grpc_hash = normalize_hash_keys(grpc_hash)

      # Verify all required fields from core are present in GRPC
      core_hash.each_key do |key|
        expect(normalized_grpc_hash).to have_key(key.to_s)
      end

      # Values should match for the common fields
      expect(normalized_core_hash['payload_id']).to eq(normalized_grpc_hash['payload_id'])
      expect(normalized_core_hash['status']).to eq(normalized_grpc_hash['status'])
      expect(normalized_core_hash['line_number']).to eq(normalized_grpc_hash['line_number'])
      expect(normalized_core_hash['type']).to eq(normalized_grpc_hash['type'])
      expect(normalized_core_hash['description']).to eq(normalized_grpc_hash['description'])
    end
  end

  describe 'Response equality' do
    let(:expected_grpc_response_fields) do
      %w[results status applied_exclusions]
    end

    let(:expected_core_response_methods) do
      [:to_h, :status, :state, :metadata, :==, :results, :applied_exclusions]
    end

    it 'GRPC::ScanResponse has expected fields' do
      fields = Gitlab::SecretDetection::GRPC::ScanResponse.descriptor.entries
      expect(fields.map(&:name)).to match_array(expected_grpc_response_fields)
    end

    it 'Core::Response has expected methods' do
      expect(Gitlab::SecretDetection::Core::Response.instance_methods(false)).to match_array(
        expected_core_response_methods
      )
    end

    it 'has matching field structure between Core::Response and GRPC::ScanResponse', :aggregate_failures do
      # Create a test finding to include in the response
      test_finding = Gitlab::SecretDetection::Core::Finding.new(
        'test_payload', Gitlab::SecretDetection::Core::Status::FOUND, 42, 'test_type', 'test_description'
      )

      test_grpc_finding = Gitlab::SecretDetection::GRPC::ScanResponse::Finding.new(
        payload_id: 'test_payload',
        status: Gitlab::SecretDetection::GRPC::ScanResponse::Status::STATUS_FOUND,
        line_number: 42,
        type: 'test_type',
        description: 'test_description'
      )

      # Create an exclusion for testing
      exclusion = Gitlab::SecretDetection::GRPC::Exclusion.new(
        exclusion_type: Gitlab::SecretDetection::GRPC::ExclusionType::EXCLUSION_TYPE_RULE,
        value: 'test_rule'
      )

      # Create instances of both response classes
      core_response = Gitlab::SecretDetection::Core::Response.new(
        status: Gitlab::SecretDetection::Core::Status::FOUND,
        results: [test_finding],
        applied_exclusions: [exclusion],
        metadata: { 'test_key' => 'test_value' }
      )

      grpc_response = Gitlab::SecretDetection::GRPC::ScanResponse.new(
        status: Gitlab::SecretDetection::GRPC::ScanResponse::Status::STATUS_FOUND,
        results: [test_grpc_finding],
        applied_exclusions: [exclusion]
      )

      # Compare the response structures
      core_hash = core_response.to_h
      grpc_hash = grpc_response.to_h

      # Core response should contain all the fields in GRPC response
      expect(core_hash[:status]).to eq(grpc_hash[:status])
      expect(core_hash[:results].length).to eq(grpc_hash[:results].length)
      expect(core_hash[:applied_exclusions].length).to eq(grpc_hash[:applied_exclusions].length)

      # Core might have additional fields (like metadata) that gRPC doesn't have
      expect(core_hash).to have_key(:metadata)
    end
  end

  describe 'Scanner response conversion' do
    it 'ensures Core::Scanner responses can be properly converted to GRPC::ScanResponse', :aggregate_failures do
      # Create sample Core::Response
      core_finding = Gitlab::SecretDetection::Core::Finding.new(
        'test_payload', Gitlab::SecretDetection::Core::Status::FOUND, 42, 'test_type', 'test_description'
      )

      exclusion = Gitlab::SecretDetection::GRPC::Exclusion.new(
        exclusion_type: Gitlab::SecretDetection::GRPC::ExclusionType::EXCLUSION_TYPE_RULE,
        value: 'test_rule'
      )

      core_response = Gitlab::SecretDetection::Core::Response.new(
        status: Gitlab::SecretDetection::Core::Status::FOUND,
        results: [core_finding],
        applied_exclusions: [exclusion]
      )

      # We don't actually call the conversion code, but we ensure the structure allows for such conversion
      findings = core_response.results.map do |finding|
        Gitlab::SecretDetection::GRPC::ScanResponse::Finding.new(
          payload_id: finding.payload_id,
          status: finding.status,
          line_number: finding.line_number,
          type: finding.type,
          description: finding.description
        )
      end

      # Create a GRPC response from the Core response
      grpc_response = Gitlab::SecretDetection::GRPC::ScanResponse.new(
        status: core_response.status,
        results: findings,
        applied_exclusions: core_response.applied_exclusions
      )

      # Verify the conversion produces a valid GRPC response
      expect(grpc_response.status).to eq(core_response.status)
      expect(grpc_response.results.length).to eq(core_response.results.length)
      expect(grpc_response.results[0].payload_id).to eq(core_response.results[0].payload_id)
      expect(grpc_response.results[0].status).to eq(core_response.results[0].status)
      expect(grpc_response.results[0].line_number).to eq(core_response.results[0].line_number)
      expect(grpc_response.results[0].type).to eq(core_response.results[0].type)
      expect(grpc_response.results[0].description).to eq(core_response.results[0].description)
      expect(grpc_response.applied_exclusions).to eq(core_response.applied_exclusions)
    end
  end

  def normalize_hash_keys(hash)
    hash.each_with_object({}) do |(k, v), result|
      result[k.to_s] = v.is_a?(Hash) ? normalize_hash_keys(v) : v
    end
  end
end
