# frozen_string_literal: true

require 'spec_helper'
require 'ostruct'

RSpec.describe 'ProjectSecurityExclusion Interface Compatibility', :integration do
  # This spec ensures that the interface expected from Security::ProjectSecurityExclusion
  # is compatible with the Exclusion class in the secret detection service,
  # allowing them to be used interchangeably in the core scanner logic.

  # Create a double that mimics the interface of Security::ProjectSecurityExclusion
  let(:project_security_exclusion_double) do
    Class.new do
      attr_reader :type, :value, :active, :scanner

      def initialize(type:, value:, active: true, scanner: 'secret_push_protection')
        @type = self.class.types[type]
        @value = value
        @active = active
        @scanner = scanner
      end

      def self.types
        {
          'path' => 0,
          'regex_pattern' => 1,
          'raw_value' => 2,
          'rule' => 3
        }
      end
    end
  end

  # We'll create instances of this double for testing
  let(:path_exclusion) do
    project_security_exclusion_double.new(
      type: 'path',
      value: 'test/path/*.rb'
    )
  end

  let(:rule_exclusion) do
    project_security_exclusion_double.new(
      type: 'rule',
      value: 'gitlab_personal_access_token'
    )
  end

  let(:raw_value_exclusion) do
    project_security_exclusion_double.new(
      type: 'raw_value',
      value: 'MY_SECRET_VALUE'
    )
  end

  let(:regex_pattern_exclusion) do
    project_security_exclusion_double.new(
      type: 'regex_pattern',
      value: '/my secret regex/'
    )
  end

  let(:expected_exclusion_fields) do
    %w[exclusion_type value]
  end

  # Intended to catch situations where the GRPC Exclusion is updated but not the PSE in the monolith
  it 'has only the expected fields' do
    fields = Gitlab::SecretDetection::GRPC::Exclusion.descriptor.entries
    expect(fields.count).to eq(expected_exclusion_fields.count)
    expect(fields.map(&:name)).to match_array(expected_exclusion_fields)
  end

  describe 'Exclusion type compatibility', :aggregate_failures do
    it 'has exclusion types that can be mapped to GRPC::ExclusionType values' do
      # ProjectSecurityExclusion types: { path: 0, regex_pattern: 1, raw_value: 2, rule: 3 }
      # GRPC::ExclusionType: EXCLUSION_TYPE_UNSPECIFIED = 0, EXCLUSION_TYPE_RULE = 1,
      #                      EXCLUSION_TYPE_RAW_VALUE = 2, EXCLUSION_TYPE_PATH = 3

      # Get the GRPC enum values and get the name as a string, e.g., EXCLUSION_TYPE_RAW_VALUE = 'raw_value'
      grpc_types = Gitlab::SecretDetection::GRPC::ExclusionType.constants.to_h do |const|
        [const.to_s.sub('EXCLUSION_TYPE_', '').downcase, Gitlab::SecretDetection::GRPC::ExclusionType.const_get(const)]
      end

      # Get our double's enum values
      gitlab_types = project_security_exclusion_double.types

      gitlab_types.each do |gitlab_type|
        name = gitlab_type.first
        expect(grpc_types).to have_key(name),
          "GRPC is missing the exclusion type: #{name}"
      end

      # The special case: regex_pattern doesn't have a direct GRPC equivalent
      expect(gitlab_types).to have_key('regex_pattern')
    end

    it 'can adapt ProjectSecurityExclusion to the format expected by secret detection service', :aggregate_failures do
      # Create a mapping for conversion, the protobuf enums become symbols
      type_mapping = {
        project_security_exclusion_double.types['path'] => :EXCLUSION_TYPE_PATH,
        project_security_exclusion_double.types['raw_value'] => :EXCLUSION_TYPE_RAW_VALUE,
        project_security_exclusion_double.types['rule'] => :EXCLUSION_TYPE_RULE,
        project_security_exclusion_double.types['regex_pattern'] => :EXCLUSION_TYPE_REGEX_PATTERN
      }

      # Convert each type of exclusion to GRPC::Exclusion format
      exclusions = [path_exclusion, rule_exclusion, raw_value_exclusion, regex_pattern_exclusion]

      exclusions.each do |exclusion|
        grpc_exclusion = Gitlab::SecretDetection::GRPC::Exclusion.new(
          exclusion_type: type_mapping[exclusion.type],
          value: exclusion.value
        )

        # Verify the conversion
        expect(grpc_exclusion.value).to eq(exclusion.value)
        expect(grpc_exclusion.exclusion_type).to eq(type_mapping[exclusion.type])
      end
    end
  end

  describe 'Field compatibility for scanner integration' do
    it 'has the necessary fields to be used by the scanner service', :aggregate_failures do
      exclusion = Gitlab::SecretDetection::GRPC::Exclusion.new(
        exclusion_type: 0,
        value: 'super secret value'
      )
      # Fields required by the scanner service based on GRPC::Exclusion
      required_fields = [:value] # we don't use `type` because the exclusions are already separated by type

      # Verify that all required fields exist on our test double
      required_fields.each do |field|
        expect(exclusion).to respond_to(field),
          "GRPC::Exclusion is missing field: #{field}"
      end

      # Verify that type returns a string that can be mapped to GRPC::ExclusionType
      expect(exclusion.value).to be_a(String)
      # we don't use the `type` field, the scanner expects a hash with the exclusions divided up by their types
    end
  end

  describe 'Bidirectional conversion' do
    it 'demonstrates a full round trip from ProjectSecurityExclusion to GRPC and back', :aggregate_failures do
      # Start with our test double
      original_exclusion = path_exclusion

      # Convert to GRPC format
      grpc_exclusion = Gitlab::SecretDetection::GRPC::Exclusion.new(
        exclusion_type: Gitlab::SecretDetection::GRPC::ExclusionType::EXCLUSION_TYPE_PATH,
        value: original_exclusion.value
      )

      # Convert back to what would be a ProjectSecurityExclusion
      reverse_mapping = {
        EXCLUSION_TYPE_PATH: 'path',
        EXCLUSION_TYPE_RAW_VALUE: 'raw_value',
        EXCLUSION_TYPE_RULE: 'rule'
      }

      # This would typically be done by an adapter
      reconstructed_exclusion = project_security_exclusion_double.new(
        type: reverse_mapping[grpc_exclusion.exclusion_type],
        value: grpc_exclusion.value
      )

      # Verify the original and reconstructed objects have the same essential data
      expect(reconstructed_exclusion.type).to eq(original_exclusion.type)
      expect(reconstructed_exclusion.value).to eq(original_exclusion.value)
    end
  end
end
