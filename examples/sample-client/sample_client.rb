# frozen_string_literal: true

require 'grpc'
require_relative '../../lib/gitlab/secret_detection/grpc'

class SampleClientRequestEnumerator
  def initialize(requests)
    @requests = requests
  end

  # yields a request, waiting between 0 and 1 seconds between requests
  #
  # @return an Enumerable that yields a request input
  def each_item
    return enum_for(:each_item) unless block_given?

    @requests.each do |request|
      yield request
    end
  end
end

# SampleClient is a sample Ruby client code to invoke the Secret Detection RPC server

class SampleClient
  REQUEST_HEADERS = { 'grpc-internal-encoding-request' => 'gzip',
                      'grpc-encoding' => 'gzip',
                      'grpc-accept-encoding' => ['gzip'],
                      'content-coding' => 'gzip',
                      'content-encoding' => 'gzip',
                      'x-sd-auth' => '12345' }.freeze

  COMPRESSION_ARGS = ::GRPC::Core::CompressionOptions.new(default_algorithm: :gzip).to_channel_arg_hash

  def initialize(connect_staging: false)
    @stub = if connect_staging
              create_staging_client_stub
            else
              create_localhost_stub
            end

    run_scan
    run_scan_stream
  rescue ::GRPC::Unavailable
    puts '**ERROR** gRPC server is not running at 50001 port. Run `make grpc_serve` in your CLI to start server.'
  end

  # Unary call
  def run_scan
    puts "[SCAN] test running ..."
    payloads = [Gitlab::SecretDetection::GRPC::ScanRequest::Payload.new(id: '1', data: "glpat-#{'a' * 20}")]

    result = @stub.scan(
      Gitlab::SecretDetection::GRPC::ScanRequest.new(payloads:),
      metadata: REQUEST_HEADERS
    )

    puts "[SCAN] result: #{result}"
  end

  # Bi-directional streaming call
  def run_scan_stream
    puts "[SCAN_STREAM] test running ..."

    payload_cls = Gitlab::SecretDetection::GRPC::ScanRequest::Payload
    payloads = [
      payload_cls.new(id: '1', data: "glpat-#{'a' * 20}"),
      payload_cls.new(id: '2', data: "dummy"),
      payload_cls.new(id: '3', data: "glpat-#{'b' * 20}"),
      payload_cls.new(id: '4', data: "dummy"),
      payload_cls.new(id: '5', data: "glpat-#{'b' * 20}"),
      payload_cls.new(id: '6', data: "dummy")
    ]

    requests = payloads.map { |payload| Gitlab::SecretDetection::GRPC::ScanRequest.new(payloads: [payload]) }

    puts "[SCAN_STREAM] #{requests.length} stream requests sent **simultaneously**"
    request_enum = SampleClientRequestEnumerator.new(requests)

    @stub.scan_stream(
      request_enum.each_item,
      metadata: REQUEST_HEADERS) do |result|
      puts "[SCAN_STREAM] stream result received - #{result.inspect}"
    end

    puts "[SCAN_STREAM] stream ended"
  end

  def create_staging_client_stub
    # Load the server certificate
    # NOTE: Although gRPC server is not configured with cert but Cloud Run
    # wraps the cloud instance with a ssl cert to support HTTPS
    server_cert = File.read(File.expand_path('staging_server.crt', __dir__))
    creds = ::GRPC::Core::ChannelCredentials.new(server_cert)
    staging_server_host = "secret-detection.staging.runway.gitlab.net:443"

    Gitlab::SecretDetection::GRPC::Scanner::Stub.new(
      staging_server_host,
      creds,
      channel_args: COMPRESSION_ARGS
    )
  end

  def create_localhost_stub
    port = ENV['RPC_SERVER_PORT'] || 50001
    localhost = "0.0.0.0:#{port}"

    Gitlab::SecretDetection::GRPC::Scanner::Stub.new(
      localhost,
      :this_channel_is_insecure,
      channel_args: COMPRESSION_ARGS
    )
  end
end

SampleClient.new connect_staging: false
