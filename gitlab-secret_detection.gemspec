# frozen_string_literal: true

require_relative 'lib/gitlab/secret_detection/version'

Gem::Specification.new do |spec|
  spec.name = "gitlab-secret_detection"
  spec.version = Gitlab::SecretDetection::Gem::VERSION
  spec.authors = ['group::secret detection', 'Stan Hu', 'gitlab_rubygems']
  spec.email = ['eng-dev-secure-secret-detection@gitlab.com', 'stan@gitlab.com']

  spec.summary = "GitLab Secret Detection gem scans for the secret leaks in the given text-based payloads."
  spec.description = "GitLab Secret Detection gem accepts text-based payloads, matches them against predefined secret
    detection rules (based on the ruleset used by GitLab Secrets analyzer), and returns the scan results. The gem also
    supports customization of the scan behaviour."

  spec.homepage = "https://gitlab.com/gitlab-org/security-products/secret-detection/secret-detection-service"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 3.2"

  spec.metadata["rubygems_mfa_required"] = "true"
  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage

  spec.metadata["changelog_uri"] =
    "https://gitlab.com/gitlab-org/security-products/secret-detection/secret-detection-service/-/blob/main/CHANGELOG.md"

  spec.files = `git ls-files config proto lib LICENSE README.md`.split("\n")
  spec.files.concat(Dir.glob('**/secret_push_protection_rules.toml'))
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'grpc', '~> 1.63'
  spec.add_runtime_dependency 'grpc_reflection', '~> 0.1'
  spec.add_runtime_dependency 'grpc-tools', '~> 1.63'
  spec.add_runtime_dependency 'parallel', '~> 1'
  spec.add_runtime_dependency 're2', '~> 2.7'
  spec.add_runtime_dependency 'sentry-ruby', '~> 5.22'
  spec.add_runtime_dependency 'stackprof', '~> 0.2.27'
  spec.add_runtime_dependency 'toml-rb', '~> 2.2'
end
