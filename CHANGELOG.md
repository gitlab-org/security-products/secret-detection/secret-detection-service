# Secret Detection Service changelog

## v0.20.4

### fixed

- Fixed invalid property values in Renovate config

### added

- CI job to validate the renovate config
- CI job to check for the latest version mismatch between CHANGELOG and Gem version

## v0.20.3

### fixed

- Matching rules are correctly attributed when filtering by tags.

## v0.20.2

### updated

- Configured keepalive args in server configuration to avoid connection closure (!63)

## v0.20.1

### fixed

- Added EXCUSION_TYPE_REGEX to match rails' `ProjectSecurityExclusions` enum.
- Removed conversion code from `GRPC` to `Core` classes in the client, opting for duck-typing instead.

## v0.20.0

### added

- Support for GitLab-integrated Error Tracking for SDS service (!58) 

## v0.19.1

### fixed

- Fix a bug where raw values weren't propagated in GRPC request (!57)
- Fix a bug where all the raw-value exclusions were considered under applied exclusions (!57)
- Fix a flaky rspec test that tests payload scan timeout (!57)

### added 

- Add exhausting logging support for troubleshooting (!57)

## v0.19.0

### added

- Make container run as non-root user (!56)
- Exclude irrelvant files and dirs from container (!56)

## v0.18.0

### updated

- Relax Gem dependencies (!55)
- Assign static version in Gemspec (!55)
- Update Ruby version to 3.2.6 (!55)

## v0.17.0

- Revert: Relax Gem dependencies (!54)

## v0.16.0

- Relax Gem dependencies (!53)

## v0.15.0

### updated

- Upgraded Secret Detection Rules version from 0.3.0 to 0.6.1

## v0.14.2

### fix

- Remove `lib/gitlab.rb` to prevent name collisions in the monolith. (!49)

## v0.14.1

### fix

- Include the `secret_push_protection_rules.toml` file in the gem. (!48)

## v0.14.0

### changed

- Use a single Hash for exclusions and internally, use the `GRPC::Exclusion` class. (!47)

## v0.13.0

### changed

- Collect applied exclusions from the secrets scan and add them to the response. (!43)

## v0.12.0

### changed

- Update scanner response to include rule title when available (!46)

## v0.11.1

## fixed

- Change `rexml` gem version to `3.3.9` to align with monolith and fix CVE-2024-41123, CVE-2024-41946, CVE-2024-43398, and CVE-2024-49761 (!40)

## v0.11.0

### added

- Added an `applied_exclusions` field to the `ScanResponse` object. (!42)

## v0.10.0

### added

- Pull secret detection rules from `gitlab-org/security-products/secret-detection/secret-detection-rules` during build time (!36)

## v0.9.0

### added

- Add support for optional `offset` property in the scan request's payload (!37)

## v0.8.0

### added

- Enable Container Scanning for Docker images (!34)

### changed

- Run analyzer scans in merge request pipeline (!34)
- Update Runway version to `v3.44.5` (!34)

## v0.7.1

### fixed

- Ruleset now allows a logger object to be passed in or defaults to a STDOUT one if none is supplied (!32)

## v0.7.0

### changed

- Change the instance resource configuration for production environment (!33)
- Update runway config version to `3.43.5` (!33)

## v0.6.1

### fixed

- Changed `GitLab` to `Gitlab` in module names to match the monolith (!30)

## v0.6.0

### added

- Support for running the scans within subprocess when used as a gem (!31)

## v0.5.0

### added

- GRPC Client to invoke Secret Detection service (!29)
- Changes grpc client integration test to refer GRPC client from Gem (!29)

### changed

- Change service definition to keep it consistent with gem's scan interface properties (!29)
- Change `grpc-tools` gem version to `1.63.0` to align with `grpc`lib (!29)

## v0.4.1

### fixed

- Change `grpc` gem version to `1.63.0` to align with monolith (!27)
- Change `re2` gem version to `2.7.0` to align with monolith (!27)
- Change `toml-rb` gem version to `2.2.0` to align with monolith (!27)

## v0.4.0

### changed

- Updated the scanner GRPC service to handle exclusions instead of allowlist entries (!28)

## v0.3.3

### fixed

- Remove the error-prone logic of verifying the released gem in RubyGems.org (!25)

## v0.3.2

### fixed

- Fix the issue of failing to publish the bundled gem in the `publish ruby gem` CI script (!24)

## v0.3.1

### changed

- Change Ruby version to `3.2.5` to sync with GitLab Rails's ruby version (!20)
- Move Runway CI configuration to `.gitlab-ci.yml` file (!20)
- Upgrade Runway version to `v3.41.2` (!20)

### fixed

- Add delay between Gem publish and Publish verification steps in CI (!20)
- Discard the gem publish job when remote gem version and the latest changelog version are same (!20)

## v0.3.0

### added

- Publish gitlab-secret_detection gem to RubyGems via GitLab CI (!19)
- Build gitlab-secret_detection ruby gem for every MR pipeline event (!19)
- Add regression test for gitlab-secret_detection ruby gem (!19)

## v0.2.0

### changed

- Upgrade Runway version to `v3.39.3` (!13)

### removed

- Remove unused runway deployment regions from Staging and Production (!13)

## v0.1.0

### added

- Introduce auto git tag and release creation on merging `main` (!10)
- Add CHANGELOG, Contributor guidelines and Codeowners  (!10)
