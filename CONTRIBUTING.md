## Developer Certificate of Origin and License

By contributing to GitLab B.V., you accept and agree to the following terms and
conditions for your present and future contributions submitted to GitLab B.V.
Except for the license granted herein to GitLab B.V. and recipients of software
distributed by GitLab B.V., you reserve all right, title, and interest in and to
your Contributions.

All contributions are subject to the
[Developer Certificate of Origin and License](https://docs.gitlab.com/ee/legal/developer_certificate_of_origin).

_This notice should stay as the first item in the CONTRIBUTING.md file._

## Code of conduct

As contributors and maintainers of this project, we pledge to respect all people
who contribute through reporting issues, posting feature requests, updating
documentation, submitting pull requests or patches, and other activities.

We are committed to making participation in this project a harassment-free
experience for everyone, regardless of level of experience, gender, gender
identity and expression, sexual orientation, disability, personal appearance,
body size, race, ethnicity, age, or religion.

Examples of unacceptable behavior by participants include the use of sexual
language or imagery, derogatory comments or personal attacks, trolling, public
or private harassment, insults, or other unprofessional conduct.

Project maintainers have the right and responsibility to remove, edit, or reject
comments, commits, code, wiki edits, issues, and other contributions that are
not aligned to this Code of Conduct. Project maintainers who do not follow the
Code of Conduct may be removed from the project team.

This code of conduct applies both within project spaces and in public spaces
when an individual is representing the project or its community.

Instances of abusive, harassing, or otherwise unacceptable behavior can be
reported by emailing contact@gitlab.com.

This Code of Conduct is adapted from the [Contributor Covenant](https://contributor-covenant.org), version 1.1.0,
available at [https://contributor-covenant.org/version/1/1/0/](https://contributor-covenant.org/version/1/1/0/).

## Project License

You can view this projects license in [LICENSE.md](LICENSE)

## Contributor Resources

Thank you for your interest in contributing to GitLab.

For a first-time step-by-step guide to the contribution process, see our
[Contributing to GitLab](https://about.gitlab.com/community/contribute/) page.

You may also find these resources useful:
- [Documentation for community contributions](https://docs.gitlab.com/ee/development/contributing/#contribute-to-gitlab)
- [Security Scanner documentation](https://docs.gitlab.com/ee/development/integrations/secure.html)

### Write small, atomic commits

Commits should be as small as possible but not smaller than required to make a
logically complete change. If you struggle to find a proper summary for your
commit message, it's a good indicator that the changes you make in this commit may
not be focused enough.

`git add -p` is useful to add only relevant changes. Often you only notice that
you require additional changes to achieve your goal when halfway through the
implementation. Use `git stash` to help you stay focused on this additional
change until you have implemented it in a separate commit.

### Split up refactors and behavioral changes

Introducing changes in behavior very often requires preliminary refactors. You
should never squash refactoring and behavioral changes into a single commit,
because that makes it very hard to spot the actual change later.

### Tell a story

When splitting up commits into small and logical changes, there will be many
interdependencies between all commits of your feature branch. If you make
changes to simply prepare another change, you should briefly mention the overall
goal that this commit is heading towards.

### Describe why you make changes, not what you change

When writing commit messages, you should typically explain why a given change is
being made. For example, if you have pondered several potential solutions, you
can explain why you settled on the specific implementation you chose. What has
changed is typically visible from the diff itself.

A good commit message answers the following questions:

- What is the current situation?
- Why does that situation need to change?
- How does your change fix that situation?
- Are there relevant resources which help further the understanding? If so,
  provide references.

You may want to set up a [message template](https://thoughtbot.com/blog/better-commit-messages-with-a-gitmessage-template)
to pre-populate your editor when executing `git commit`.

### Use scoped commit subjects

Many projects typically prefix their commit subjects with a scope. For example,
if you're implementing a new feature "X" for subsystem "Y", your commit message
would be "Y: Implement new feature X". This makes it easier to quickly sift
through relevant commits by simply inspecting this prefix.

### Keep the commit subject short

Because commit subjects are displayed in various command line tools by default,
keep the commit subject short. A good rule of thumb is that it shouldn't exceed
72 characters.

### Use interactive rebases to arrange your commit series

Use interactive rebases to end up with commit series that are readable and
therefore also easily reviewable one-by-one. Use interactive rebases to
rearrange commits, improve their commit messages, or squash multiple commits
into one.

### Ensure that all commits build and pass tests

Ensure the tests are passing before submitting the merge request. You can run all the tests
in the project using following `make` command:

```shell
$ make run_all_tests
```

### Changelog

The project keeps a [changelog](CHANGELOG.md) that is used when a new release is created.

The CHANGELOG entries should be under one of the following formats:

- `added`
- `fixed`
- `changed`
- `deprecated`
- `removed`
- `security`
- `performance`
- `other`

Example:

```plaintext

## v1.2.3

### security

- Upgrade `glibc` dependency version to `2.5.2` to resolve CVE-2691 vulnerability (!189) 

```

where `189` is the GitLab Merge Request ID that resolves the vulnerability.

### Issue tracker

To get support for your particular problem please use the
[getting help channels](https://about.gitlab.com/getting-help/).

The [GitLab issue tracker on GitLab.com][gitlab-tracker] is the right place for bugs and feature proposals about Security Products.
Please use the ~"section::sec", ~"devops::secure", and ~"group::secret detection" labels when opening a new issue to ensure it is
quickly reviewed by the right people.

**[Search the issue tracker][gitlab-tracker]** for similar entries before
submitting your own, there's a good chance somebody else had the same issue or
feature proposal. Show your support with an award emoji and/or join the
discussion.

Not all issues will be addressed and your issue is more likely to
be addressed if you submit a merge request which partially or fully solves
the issue. If it happens that you know the solution to an existing bug, please first
open the issue in order to keep track of it and then open the relevant merge
request that potentially fixes it.

[gitlab-tracker]: https://gitlab.com/gitlab-org/gitlab/issues

## Contributor Code of conduct

We want to create a welcoming environment for everyone who is interested in contributing.
Visit our [Code of Conduct page](https://about.gitlab.com/community/contribute/code-of-conduct/)
to read our community pledge and standards.
