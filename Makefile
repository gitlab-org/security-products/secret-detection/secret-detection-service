.PHONY: gem_clean gem_build install_secret_detection_rules

install:
	bundle install

lint_fix:
	bundle exec rubocop -A .

run_grpc_tests: install_secret_detection_rules
	bundle exec rspec -f d spec/lib/gitlab/secret_detection/grpc/*_spec.rb

run_all_tests: install_secret_detection_rules
	bundle exec rspec -f d .

run_core_tests: install_secret_detection_rules
	bundle exec rspec -f d ./spec/lib/gitlab/secret_detection/core/*_spec.rb

run_utils_tests:
	bundle exec rspec -f d ./spec/lib/gitlab/secret_detection/utils/*_spec.rb

# =========================================================
# Ruby Gem
# =========================================================
gem_clean:
	rm gitlab-secret_detection-*.gem > /dev/null 2>&1 || true

gem_build: gem_clean install_secret_detection_rules
	gem build "gitlab-secret_detection.gemspec"

# =========================================================
# GRPC service
# =========================================================
PROTO_DIR:=${PWD}/proto
GENERATED_FILES_PATH:=${PWD}/lib/gitlab/secret_detection/grpc/generated

# Generate Ruby protobuf files
generate_proto: install
	bundle exec grpc_tools_ruby_protoc \
		-I ${PROTO_DIR} \
		--ruby_out=${GENERATED_FILES_PATH} \
		--grpc_out=${GENERATED_FILES_PATH} \
		${PROTO_DIR}/*.proto

grpc_docker_build:
	docker build -f Dockerfile -t secret_detection_grpc_svc .

grpc_docker_serve:
	docker run -p 8080:8080 secret_detection_grpc_svc

grpc_serve:
	bundle exec ruby bin/start_server

# Copy secret-detection-rules based on version defined in RULES_VERSION
install_secret_detection_rules:
	sh ci/scripts/install_secret_detection_rules.sh
