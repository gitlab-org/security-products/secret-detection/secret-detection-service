#!/bin/bash

# This script downloads the version of the secret-detection-rules package specified in the RULES_VERSION file and places that file in the appropriate directory.

SECRET_DETECTION_RULES_PROJECT_ID=60960406
SECRET_DETECTION_RULES_VERSION=$(cat RULES_VERSION)

PACKAGE_URL="https://gitlab.com/api/v4/projects/${SECRET_DETECTION_RULES_PROJECT_ID}/packages/generic/secret-detection-rules/v${SECRET_DETECTION_RULES_VERSION}/secret-detection-rules-v${SECRET_DETECTION_RULES_VERSION}.zip"

wget "$PACKAGE_URL" && \
	unzip secret-detection-rules-v"${SECRET_DETECTION_RULES_VERSION}".zip && \
	rm secret-detection-rules-v"${SECRET_DETECTION_RULES_VERSION}".zip && \
	mv dist/secret_push_protection_rules.toml "${PWD}"/lib/gitlab/secret_detection/core/secret_push_protection_rules.toml && \
	rm -r dist && \
	echo "Using rules from https://gitlab.com/gitlab-org/security-products/secret-detection-rules/-/tree/v${SECRET_DETECTION_RULES_VERSION}" ; \

