#!/bin/bash

if [[ -z "${GEM_HOST_API_KEY}" ]]; then
  echo "GEM_HOST_API_KEY is not set!"
  exit 1
fi

gemfile=$(ls "$CI_PROJECT_DIR" | grep ".gem$")
if [[ -z "${gemfile}" ]]; then
  echo "ERROR: The project directory does not contain a bundled gem file(.gem) to publish!"
  exit 1
fi

gemfile_path="$PWD/$gemfile"

# Add dependencies
apk add --no-progress yq-go

release_version=$(gem spec "$gemfile_path" version | yq '.version')
echo "Gem version to publish to RubyGems.org: $release_version"

version_before_publish=$(gem spec --remote gitlab-secret_detection version | yq '.version')
echo "Latest gem version at RubyGems.org before publish: $version_before_publish"

if [[ "$release_version" = "$version_before_publish" ]]; then
  echo "The gem version to publish is the same as the latest version on RubyGems.org, so skipping the gem publish action!"
  exit 0
else
  echo "Publishing gitlab-secret_detection gem(version:$release_version) to RubyGems.org .."
fi

# publish to RubyGems.org
gem push "$gemfile_path"
publish_status=$? # capture gem push exit status

if [[ $publish_status -ne 0 ]]; then
  echo "ERROR: Failed to publish the bundled gem to RubyGems.org!"
  exit 1
else
  echo "Gem published successfully to RubyGems.org!"
fi

# TODO: Add verification logic to check if the gem is correctly published to RubyGems.org
# See: https://gitlab.com/gitlab-org/gitlab/-/issues/494681