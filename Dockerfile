# Use the official Ruby base image
FROM ruby:3.2

ENV RPC_SERVER_PORT=8080

# Upgrade
RUN apt update && \
    apt upgrade --yes && \
    groupadd -r gitlab && \
    useradd --no-log-init -r -g gitlab gitlab

# Set the working directory in the container
WORKDIR /usr/src

# Copy the application code into the container
COPY --chown=gitlab:gitlab . .

# Add gitlab group and user
# Install the gem dependencies
RUN bundle install

# Install secret detection ruleset
RUN make install_secret_detection_rules

USER gitlab

# Expose the port where the RPC Server is listening
EXPOSE ${RPC_SERVER_PORT}

# Define the command to run the application
CMD ["make", "grpc_serve"]
