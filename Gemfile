# frozen_string_literal: true

source "https://rubygems.org"

gemspec

# relative load all the files
Dir.glob(File.join(__dir__, '**')) do |file_path|
  next unless File.directory?(file_path)

  $LOAD_PATH.unshift(file_path)
end

if ENV.fetch('BUNDLER_CHECKSUM_VERIFICATION_OPT_IN', 'false') != 'false' # this verification is still experimental
  $LOAD_PATH.unshift(File.expand_path("./vendor/gems/bundler-checksum/lib", __dir__))
  require 'bundler-checksum'
  BundlerChecksum.patch!
end

group :development, :test do
  gem "benchmark-malloc", "~> 0.2", require: false
  gem 'gitlab-styles', '~> 12.0', '>= 12.0.1', require: false
  gem "rspec", "~> 3.13", require: false
  gem "rspec-benchmark", "~> 0.6.0", require: false
  gem "rspec-parameterized", "~> 1.0", require: false
  gem "rubocop", "~> 1.62", require: false
  gem "rubocop-rspec", "~> 2.27", require: false
  gem "lefthook", "~> 1.7", require: false
  gem 'bundler-checksum', '~> 0.1.0', path: 'vendor/gems/bundler-checksum', require: false
  gem 'pry-byebug', require: false
  gem 'simplecov', require: false
end
